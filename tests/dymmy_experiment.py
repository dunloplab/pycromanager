# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 14:31:15 2022

@author: System 6
"""

import pycromanager_tessie as pmt

# Start platform:
platform = pmt.microscope.platform.Platform(load_synchronizer=True)

# Set LED ring
#platform.neopixel.color(red=int(255/4))

#%% Calibrate DMD:
platform.neopixel.shutter(True)
platform.dmd.calibrate(color = 'blue')
platform.neopixel.shutter(False)

#%% Experimental parameters:

# Init experiment object:
xp = pmt.acquisitions.experiment.TestExperiment(
    platform,
    delta_config = "C:/Users/System 6/Documents/config_mothermachine.json"
    )
xp.save_folder = "E:/JBL/dummy_tests/test_01"
xp.stim_batches = 5
xp.period = 60
xp._run_delta = False

# Channels:
trans_settings = pmt.acquisitions.series.ChannelSettings(
    name='Trans', exposure=15, trigger = "Ext"
    )
gfp_settings = pmt.acquisitions.series.ChannelSettings(
    name='GFP', exposure=15, trigger = "Ext"
    )

# Stimulations:
reddmd_settings = pmt.acquisitions.series.StimulationsSettings(
    color='red', exposure = 1000, intensity = 100, trigger = "Ext"
    )

# Acquisition series settings: (all using the same settings here)
series_settings = pmt.acquisitions.series.SeriesSettings(
    acquisition_mode = 'simultaneous',
    channels = [trans_settings, gfp_settings],
    stimulations = [reddmd_settings]
    )

# Add series
NUM_SERIES = 2
total_series = 0
for s in range(NUM_SERIES):
    
    # Create single acquisition series:
    series = pmt.acquisitions.series.Series(
        series_number=s, platform=platform, settings = series_settings
        )
    
    # Get line of positions:
    series.positions_line(index_offset=total_series)
    
    total_series+= len(series.positions)
    
    xp.series.append(series)

#%% Pre-process positions (ie acquire reference images & init processors):
xp.preprocess()

# Assign control objectives to each cell / chamber:

#%% Run experiment:
xp.run()
