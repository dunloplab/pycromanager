# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 14:46:18 2021

@author: jeanbaptiste
"""
import gc
import time
import sys
# Append paths to delta-dev and deep cell control before import:
sys.path.append('C:/DeepLearning/DeLTA_dev')
sys.path.append('C:/Users/System 6/Documents/deepcellcontrol')
sys.path.append('C:/Users/System 6/Documents/pycromanager')

import numpy as np
import matplotlib.pyplot as plt
import cv2
import tensorflow as tf
import delta
from deepcellcontrol.control import BinaryParticleSwarmOptimizer, LSTMMPC
from deepcellcontrol.server import Server
from deepcellcontrol.data import normalization
    
delta.config.load_config(presets='mothermachine')

import pycromanager_tessie as pmt

from spyder_kernels.utils.iofuncs import load_dictionary
sys.path.append('C:/Users/System 6/Documents')
import deepmpc
d = load_dictionary('D:/2021-10-08_DeepMPC_1/dmpc.spydata')[0]



physical_devices = tf.config.list_physical_devices('GPU')

# tf.config.set_logical_device_configuration(
#   physical_devices[0],
#   [tf.config.LogicalDeviceConfiguration(memory_limit=5000),
#    tf.config.LogicalDeviceConfiguration(memory_limit=1600)])

logical_devices = tf.config.list_logical_devices('GPU')

# Load U-Net models:
models=delta.utilities.loadmodels()

# Experimental parameters:
HORIZON = 24
PAST_STEPS = 36
DELTA_FEATURES = ('fluo1',)
CTRL_FEATURES = ('fluo1','stims')

# Set up control server:
optimizer = BinaryParticleSwarmOptimizer(
    horizon=HORIZON, iterations=20, particles=20
    )
controller = LSTMMPC(
    model_file = 'C:/Users/System 6/Documents/deepcellcontrol/assets/models/lstm/batch_train_2021-10-15/2021-10-15_18-47-13/model.hdf5',
    features=len(CTRL_FEATURES),
    latent_dim=64,
    horizon=HORIZON, strategy_optimizer=optimizer, past_steps=PAST_STEPS
    )
control_server = Server(controller)
control_server.device = logical_devices[0]
# control_server.start()

# testing parameters:
NUM_POS = 3
xpfolder = 'D:/2021-10-08_DeepMPC_1/'

# Initialize position processors:
processors = []
for pos_nb in range(NUM_POS):

    # Init:
    processor = pmt.acquisitions.processors.MotherFeedback(
        position_nb=pos_nb,
        delta_models=models,
        channels=('Trans','GFP'),
        features = DELTA_FEATURES,
        normalization_fn = normalization,
        control_server = control_server,
        device = logical_devices[0]
        )
    processors.append(processor)
    
    # Preprocess:
    I0 = cv2.imread(
        xpfolder+'ref_img/Position%06d.tif'%(pos_nb+1),cv2.IMREAD_ANYDEPTH
        )
    processor.preprocess(I0)
    
    # Start daemon:
    # processor.start()

# Remove preprocessing U-Net from memory (not sure if useful):
models['rois'] = None
gc.collect()

# Generate control objective:
objective = (1750+750*np.sin(np.arange(0,24,1/12)*2*np.pi/6))/4095
for processor in processors:
    for mother in processor.mothers:
        mother['objective'] = objective

# Run through "timepoints":
for frame_nb in range(292):
    
    print("Frame %d"%frame_nb)
    # Run through positions:
    tproc = 0.
    for pos_nb in range(NUM_POS):
        
        # "Acquire" images:
        I = cv2.imread(
            xpfolder+'chan01_img/Position%06d_Frame%06d.tif'%(pos_nb+1,frame_nb+1),
            cv2.IMREAD_ANYDEPTH
            )
        G = cv2.imread(
            xpfolder+'chan02_img/Position%06d_Frame%06d.tif'%(pos_nb+1,frame_nb+1),
            cv2.IMREAD_ANYDEPTH
            )
        
        # Pass images to position processor:
        processor = processors[pos_nb]
        processor.new_image(I, 'Trans')
        processor.new_image(G, 'GFP')
        
        # time.sleep(.3)
        
        # For debugging, use this to run sequentially and get access to errors
        t0 = time.perf_counter()
        processor.process_image(I,0,frame_nb)
        t1 = time.perf_counter()
        tproc+=t1-t0
    print("Frame %d - Image processing time: %f (%f/pos)"%(frame_nb, tproc, tproc/NUM_POS))
    
    # For debugging, use this to run sequentially and get access to errors
    t0 = time.perf_counter()
    control_server._run()
    t1 = time.perf_counter()
    print("Frame %d - Control time: %f (%f/pos)"%(frame_nb, t1-t0, (t1-t0)/NUM_POS))
    
    print("WAIT %d ##################%s######################"%(frame_nb, time.ctime()))
    donepos = []
    while len(donepos) < NUM_POS:
        for pos_ind in range(NUM_POS):
            if (pos_ind not in donepos) and (not processors[pos_ind]._stims_retrieved['red']):
                donepos += [pos_ind]
                redstim = processors[pos_ind].stimulation('red', timeout=120)
                print(
                      "%s, Position %d - red stims for frame %d: "%(
                          time.ctime(), processors[pos_ind].delta_position.position_nb, frame_nb
                          ),
                      end = ''
                      )
                for mother in processors[pos_ind].mothers:
                    print(int(mother['stims'][-1]),end = ' ')
                print("")
                greenstim = processors[pos_ind].stimulation('green', timeout=120)
        time.sleep(.1)
    
    
    print("END %d ##################%s######################"%(frame_nb, time.ctime()))
    
    # # print("WAIT %d ##################%s######################"%(frame_nb, time.ctime()))
    # # Wait for dmd images:
    # for pos_ind in range(10):
    #     redstim = processors[pos_ind].stimulation('red', timeout=120)
    #     print(
    #           "%s, Position %d - red stims for frame %d: "%(
    #               time.ctime(), processors[pos_ind].delta_position.position_nb, frame_nb
    #               ),
    #           end = ''
    #           )
    #     # Run through strategies, update dmd images:
    #     for mother in processors[pos_ind].mothers:
    #         print(mother['stims'][-1],end = ' ')
    #     print("")
    #     greenstim = processors[pos_ind].stimulation('green', timeout=120)
    
    # # Show images
    # plt.imshow(
    #     np.stack(
    #         (redstim,greenstim,delta.utilities.rangescale(I, (0,255)).astype(np.uint8)),
    #         axis=2,
    #         )
    #     )
    # plt.show()
    
    # # print("HALF %d ##################%s######################"%(frame_nb, time.ctime()))
    
    # for pos_ind in range(10,20):
    #     redstim = processors[pos_ind].stimulation('red', timeout=120)
    #     print(
    #           "%s, Position %d - red stims for frame %d: "%(
    #               time.ctime(), processors[pos_ind].delta_position.position_nb, frame_nb
    #               ),
    #           end = ''
    #           )
    #     # Run through strategies, update dmd images:
    #     for mother in processors[pos_ind].mothers:
    #         print(mother['stims'][-1],end = ' ')
    #     print("")
    #     greenstim = processors[pos_ind].stimulation('green', timeout=120)
    
    # # print("END %d ##################%s######################"%(frame_nb, time.ctime()))
