# -*- coding: utf-8 -*-
"""
Created on Fri Mar  4 14:43:04 2022

@author: System 6
"""
import os
import sys
# Append paths to delta-dev and deep cell control before import:
sys.path.append('C:/DeepLearning/DeLTA_dev')
sys.path.append('C:/Users/System 6/Documents/deepcellcontrol')
sys.path.append('C:/Users/System 6/Documents/pycromanager')
import time
import json

import matplotlib.pyplot as plt
import numpy as np

import pycromanager_tessie as pmt

platform = pmt.microscope.platform.Platform(load_synchronizer=True)


#%%

# Set these once (DO NOT use MM to do snap or live imaging after this!):
platform.synchronizer.set_camera()
platform.synchronizer.set_fluo_lamp()
platform.synchronizer.set_dmd()

# Load dummy image sequence into DMD: (to do AFTER set_dmd())
pimg = platform.dmd.point_images()

# CAREFUL WITH THE TRIGGER ARGUMENT!
platform.dmd.sequence(pimg[0], "red", 60, intensity=100, trigger="Ext")

platform.channel("GFP")

# Keep same exposure for both channels ideally:
platform.exposure(30)

# Acquire 2 iamges:
platform.synchronizer.snap("trans")
while not platform.synchronizer.camera_ready():
    time.sleep(.0005)
platform.synchronizer.snap("gfp")
while not platform.synchronizer.camera_ready():
    time.sleep(.0005)
T = platform.retrieve()
G = platform.retrieve()

# Stimulation sequence:
platform.synchronizer.stimulation_sequence_start()
for i in range(100):
    while not platform.synchronizer.dmd_ready():
        time.sleep(.0005)
    platform.synchronizer.stimulation_sequence_step("red", 60)

platform.synchronizer.unset_camera()
platform.synchronizer.unset_fluo_lamp()
platform.synchronizer.unset_dmd()
# You can now use snap or live imaging in MM

platform.dmd.stop_sequence()
