# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 14:18:03 2021

@author: jeanbaptiste
"""

import matplotlib.pyplot as plt
import deepcellcontrol as dcc
import delta

from pycromanager_tessie.acquisitions import experiment, processors

delta.cfg.load_config("updateme")

#%% Init experiment:
xp = experiment.MotherMachineControl()
xp.save_folder = "E:/JBL/dummy_tests/test6/"

# Set LED ring:
xp.platform.neopixel.color(red=255)

# Set stimulations batch size:
xp.stim_batches = None

#%% DMD calibration:
xp.platform.neopixel.shutter(True)
xp.platform.dmd.calibrate(color = 'blue')
xp.platform.neopixel.shutter(False)

#%% Init series:
xp.interactive()

#%% Add controllers to positions in series:

# Instantiate controller:
controller = dcc.control.MLPMPC(
    model_file = 'C:/Users/System 6/Documents/deepcellcontrol/assets/models/mlp_scc/2022-01-23_19-30-13/model.hdf5',
    features=2,
    hidden_layers=10,
    horizon=24,
    past_steps=36,
    strategy_optimizer=dcc.control.BinaryParticleSwarmOptimizer(
        horizon=24, iterations=20, particles=20
        )
    )

# set up control server:
xp.control_server = dcc.server.Server(controller)
xp.control_server.device = '/device:GPU:0' # What device to run computations
xp.control_server.start() # Start server

#%% Pre-process positions:    
xp.preprocess()

#%% Assign objectives:
for series in xp.series:
    for position in series.positions:
        processor = position.processors['controller']
        for mother in processor.mothers:
            mother['objective'] = dcc.utilities.sine_objective(offset=1250)/4095
    
    # Start processing daemons:
    for pos, position in enumerate(series.positions):
        position.processors['filesaver'].start()
        position.processors['controller'].start()

#%%
#TODO set camera to 12bit low noise and Spectra to 50% (put that in settings?)
# Write somewhere that 12bit high cap seems to be faster

xp.check()
xp.platform.neopixel.color(green=1)

# Run experiment:
xp.run()

# Get images from camera after platform move and focus?
