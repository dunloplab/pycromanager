# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 14:18:03 2021

@author: jeanbaptiste
"""

# Not necessary once these are added to the python path permanently:
import sys
# Append paths to delta-dev and deep cell control before import:
sys.path.append('C:/DeepLearning/DeLTA_dev')
sys.path.append('C:/Users/System 6/Documents/deepcellcontrol')
sys.path.append('C:/Users/System 6/Documents/pycromanager')
import time

import matplotlib.pyplot as plt

import pycromanager_tessie as pmt
from pycromanager_tessie.acquisitions.series import ChannelSettings, StimulationsSettings, SeriesSettings
from pycromanager_tessie.acquisitions.processors import SimpleImageSaver, MotherFeedback
import deepcellcontrol as dcc
import delta
delta.config.load_config(presets='mothermachine')
delta_models=delta.utilities.loadmodels()

#%% Set up control server:
# TODO update parameters

# First, initialize controller object:
# controller = dcc.control.LSTMMPC(
#     model_file = 'C:/Users/System 6/Documents/deepcellcontrol/assets/models/lstm/batch_train_2021-10-15/2021-10-15_18-47-13/model.hdf5',
#     features=2,
#     latent_dim=64,
#     horizon=24,
#     past_steps=36,
#     strategy_optimizer=dcc.control.BinaryParticleSwarmOptimizer(
#         horizon=24, iterations=20, particles=20
#         )
#     )

controller = dcc.control.MLPMPC(
    model_file = 'C:/Users/System 6/Documents/deepcellcontrol/assets/models/mlp_scc/2022-01-23_19-30-13/model.hdf5',
    features=2,
    hidden_layers=10,
    horizon=24,
    past_steps=36,
    strategy_optimizer=dcc.control.BinaryParticleSwarmOptimizer(
        horizon=24, iterations=20, particles=20
        )
    )

#pass controller to control server:
control_server = dcc.server.Server(controller)
control_server.device = '/device:GPU:0' # What device to run computations
control_server.start() # Start server

# Create control objective (here all cells get the same objective):
objective = dcc.utilities.sine_objective(offset=1250)/4095

#%% Init platform:
platform = pmt.microscope.platform.Platform()

time.sleep(3) # Wait a bit after init before sending commands to neopixel
platform.neopixel.color(red=255)

#%% First find channels etc then run DMD calibration:
platform.neopixel.shutter(True)
platform.dmd.calibrate(color = 'blue')
platform.neopixel.shutter(False)

#%% Init positions and series:

# Channels:
trans_settings = ChannelSettings(name = 'Trans', exposure = 10)
gfp_settings = ChannelSettings(name = 'GFP', exposure = 50)

# Stimulations:
reddmd_settings = StimulationsSettings(color='red', exposure = 60, intensity = 20)

# Acquisition series settings: (all using the same settings here)
series_settings = SeriesSettings(
    acquisition_mode = 'simultaneous',
    channels = [trans_settings, gfp_settings],
    stimulations = [reddmd_settings]
    )

series_list = []
total_pos = 0
for s in range(1):
    
    # Create single acquisition series:
    acq_series = pmt.acquisitions.series.Series(
        series_number=s,
        platform=platform,
        settings = series_settings
        )
    
    
    # Get line of positions:
    acq_series.positions_line()
    
    # Assign processors to each position:
    channel_names = [x.name for x in acq_series.settings.channels]
    for pos, position in enumerate(acq_series.positions):
        
        
        position.processors = {
            'filesaver' : SimpleImageSaver(
                "E:/JBL/dummy_tests/test3",
                position_nb = total_pos,
                channels = channel_names,
                watch_channels = tuple(range(len(channel_names)))
                ),
            'controller' : MotherFeedback(
                position_nb = total_pos,
                name = "control_%d"%total_pos,
                channels = channel_names,
                watch_channels = (0,), # Only watch first channel
                delta_models=delta_models, # delta U-Nets
                features = ('fluo1',), # Features to extract and feed into the controller
                normalization_fn = dcc.data.normalization, # make sure we use the same data normalization
                control_server = control_server, # Control server object
                device = '/device:GPU:0' # Device to run the delta analysis on
                )
            }
        
        total_pos +=1
    
    print(acq_series)
    
    acq_series.preprocess()
    
    # Assign objective:
    for position in acq_series.positions:
        processor = position.processors['controller']
        for mother in processor.mothers:
            mother['objective'] = objective
    
    acq_series.check()
    
    for pos, position in enumerate(acq_series.positions):
        position.processors['filesaver'].start()
        position.processors['controller'].start()
    
    series_list.append(acq_series)

#%% to restart:

for series in series_list:
    series.reset()

#%% Single acq:

# for f in range(36):
    
t_start = time.perf_counter()
for series in series_list:
    series.acquire()
print(time.perf_counter()-t_start)
    

# for position in series.positions:
#     position.processors["controller"]._run()

#%%

control_server._run()

#%% 

for p, position in enumerate(series.positions):
    position.processors["controller"].busy = False
    
#%% 
platform.studio.live().set_live_mode(False) # put as a method somewhere
stim_batch = 5  

# TODO check that the drift is corrected properly
# platform.neopixel.color(green=1)
frame_nb= 1

while frame_nb <37:
    
    print("\n\n\n-------- Starting Frame %d"%frame_nb)
    t_start = time.perf_counter()
    for series in series_list:
        
        series.acquire()
        
        for i in range(0,len(series.positions),stim_batch):
            j = min(i+stim_batch,len(series.positions))
            series.stimulate(positions = list(range(i,j)))
    
    print("Frame %d"%frame_nb)
    print("Total acq time: %.1f"%(time.perf_counter()-t_start))
    
    for position in series.positions:
        position.processors["controller"]._run()
    
    frame_nb+=1
    # while time.perf_counter()-t_start < 300:
    #     time.sleep(.1)
    countdown = 5
    while(countdown):
        print(countdown)
        time.sleep(1)

# Save images to disk during DMD loading? (ie make the call to DMD load non-blocking with a daemon or something?)

#%% lib issues:
import importlib
importlib.reload(pmt)
from pycromanager_tessie.acquisitions.processors import SimpleImageSaver, MotherFeedback

#%%
mothers = []
for series in series_list:
    mothers.append([])
    for pos in series.positions:
        mothers[-1].append([])
        for mother in pos.processors["controller"].mothers:
            mothers[-1][-1].append(mother)

import pickle
with open("mothers.pkl","wb") as f:
    pickle.dump(mothers,f)