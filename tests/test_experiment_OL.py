# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 14:18:03 2021

@author: jeanbaptiste
"""

# Not necessary once these are added to the python path permanently:
import sys
# Append paths to delta-dev and deep cell control before import:
sys.path.append('C:/DeepLearning/DeLTA_dev')
sys.path.append('C:/Users/System 6/Documents/deepcellcontrol')
sys.path.append('C:/Users/System 6/Documents/pycromanager')
import time
import json

import matplotlib.pyplot as plt
import numpy as np

import pycromanager_tessie as pmt
from pycromanager_tessie.acquisitions.series import ChannelSettings, StimulationsSettings, SeriesSettings
from pycromanager_tessie.acquisitions.processors import SimpleImageSaver, MotherOpenLoop
import deepcellcontrol as dcc
import delta
delta.config.load_config(presets='mothermachine')
delta.config.model_file_seg = "C:/Users/System 6/Downloads/unet_seg_moma_3lvl.hdf5"
delta_models=delta.utilities.loadmodels()

#%% Init platform:
platform = pmt.microscope.platform.Platform()

platform.neopixel.color(red=255)


#%% First find channels etc then run DMD calibration:
platform.neopixel.shutter(True)
platform.dmd.calibrate(color = 'blue')
platform.neopixel.shutter(False)

#%% Init positions and series:

xp_folder = "E:/JBL/dummy_tests/test7_OL/"


series_list = []
total_pos = 0
for s in range(4):
    
    # Channels:
    trans_settings = ChannelSettings(name = 'Trans', exposure = 10)
    gfp_settings = ChannelSettings(name = 'GFP', exposure = 30)

    # Stimulations:
    reddmd_settings = StimulationsSettings(color='red', exposure = 60, intensity = 100)

    # Acquisition series settings: (all using the same settings here)
    series_settings = SeriesSettings(
        acquisition_mode = 'simultaneous',
        channels = [trans_settings, gfp_settings],
        stimulations = [reddmd_settings]
        )
    
    # Create single acquisition series:
    acq_series = pmt.acquisitions.series.Series(
        series_number=s,
        platform=platform,
        settings = series_settings
        )
    
    # Get line of positions:
    acq_series.positions_line()
    
    # Assign processors to each position:
    channel_names = [x.name for x in acq_series.settings.channels]
    for pos, position in enumerate(acq_series.positions):
        
        position.processors = {
            'filesaver' : SimpleImageSaver(
                xp_folder,
                position_nb = total_pos,
                channels = channel_names,
                watch_channels = tuple(range(len(channel_names)))
                ),
            'controller' : MotherOpenLoop(
                position_nb = total_pos,
                channels = channel_names,
                watch_channels = (0,), # Only watch first channel
                delta_models=delta_models, # delta U-Nets
                device = '/device:GPU:0' # Device to run the delta analysis on
                )
            }
        
        total_pos +=1
    
    print(acq_series)

    series_list.append(acq_series)


for acq_series in series_list:   
    acq_series.preprocess()
    

# Count number of chambers:
num_chambers = 0
for acq_series in series_list:
    for position in acq_series.positions:
        num_chambers += len(position.processors["controller"].mothers)
print("%d chambers"%num_chambers)

stims = dcc.utilities.random_stimulations(total_simulations=num_chambers).astype(np.int8)
# Assign 2*100 "reference" cells to go through pre-set cycles:
references = np.random.choice(num_chambers, 200, replace = False)
stims[references[:100],:] = 36*[False] + (8*12*[True] + 8*12*[False])*2 + 12*[True]
stims[references[100:],:] = 36*[False] + (8*12*[False] + 8*12*[True])*2 + 12*[False]

num_chambers = 0
for acq_series in series_list:
    # Assign stimulations::
    for position in acq_series.positions:
        processor = position.processors['controller']
        for mother in processor.mothers:
            mother['stims'] = stims[num_chambers]
            num_chambers += 1
    
    acq_series.check()
    
for acq_series in series_list:
    for pos, position in enumerate(acq_series.positions):
        position.processors['filesaver'].start()
        position.processors['controller'].start()

# #%% to restart:

# for series in series_list:
#     series.reset()

#%% 
# TODO correct this
for acq_series in series_list:
    # Assign stimulations::
    for position in acq_series.positions:
        position._control_processor = position.processors['controller']

#%% 
platform.studio.live().set_live_mode(False) # put as a method somewhere
stim_batch = 5

#TODO automatically create XP folder if doesn't exist
# Write all device properties to json file:
with open(xp_folder+"device_properties.json","w") as f:
    json.dump(platform.get_all_properties(), f, indent=4)

# Write series to txt file:
with open(xp_folder+"series.txt","w") as f:
    for series in series_list:
        f.write(str(series))
        f.write("\n"+"="*80+"\n\n\n\n")

platform.neopixel.color(green=1)
frame_nb= 1

while 1:
    
    print("\n\n\n-------- Starting Frame %d"%frame_nb)
    t_start = time.perf_counter()
    for series in series_list:
        
        series.acquire()
        
        
        for i in range(0,len(series.positions),stim_batch):
            j = min(i+stim_batch,len(series.positions))
            series.stimulate(positions = list(range(i,j)))
    
    print("Frame %d"%frame_nb)
    print("Total acq time: %.1f"%(time.perf_counter()-t_start))
    frame_nb+=1
    while time.perf_counter()-t_start < 300:
        time.sleep(.1)

# Save images to disk during DMD loading? (ie make the call to DMD load non-blocking with a daemon or something?)

#%% lib issues:
import importlib
importlib.reload(pmt)
from pycromanager_tessie.acquisitions.processors import SimpleImageSaver, MotherFeedback, MotherOpenLoop

#%%
mothers = []
for series in series_list:
    mothers.append([])
    for pos in series.positions:
        mothers[-1].append([])
        for mother in pos.processors["controller"].mothers:
            mothers[-1][-1].append(mother)

import pickle
with open(xp_folder+"mothers.pkl","wb") as f:
    pickle.dump(mothers,f)

#%%

import numpy as np

motarr = []
stimarr = []
for s in mothers:
    for p in s:
        for c in p:
            motarr += [c["fluo1"]]
            stimarr += [c["stims"]]
                
motarr = np.array(motarr)
stimarr = np.array(stimarr)

#%%
motarr1[motarr1==0] = np.nan
plt.plot(list(range(36,226)),objective[:226-36])
plt.plot(np.nanmedian(motarr1, axis=0))
plt.ylim(0,1)
plt.show()

motarr2[motarr2==0] = np.nan
plt.plot(list(range(36,226)),objective[:226-36]/2)
plt.plot(np.nanmedian(motarr2, axis=0))
plt.ylim(0,1)
plt.show()

#%%

i = np.random.randint(3300)
plt.fill_between(list(range(len(stimarr[i]))), stimarr[i]*65535,color='g')
plt.plot(motarr[i])

plt.show()

# TODO save Z

f