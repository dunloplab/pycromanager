# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 19:01:53 2022

@author: System 6
"""

import delta
import deepcellcontrol as dcc
import pycromanager_tessie as pmt

# Start platform:
platform = pmt.microscope.platform.Platform(load_synchronizer=True)

#%% Experimental parameters:
controller = dcc.control.MLPMPC(
    model_file = 'C:/Users/System 6/Documents/deepcellcontrol/assets/models/mlp_scc/2022-01-23_19-30-13/model.hdf5',
    features=2,
    hidden_layers=10,
    horizon=24,
    past_steps=36,
    strategy_optimizer=dcc.control.BinaryParticleSwarmOptimizer(
        horizon=24, iterations=20, particles=20
        )
    )

#pass controller to control server:
control_server = dcc.server.Server(controller)
control_server.device = '/device:GPU:0' # What device to run computations
control_server.start() # Start server

# Init experiment object:
xp = pmt.acquisitions.experiment.MotherMachineControl(platform)
xp.stim_batches = 5

# Channels:
trans_settings = pmt.acquisitions.series.ChannelSettings(
    name='Trans', exposure=15, trigger = "Ext"
    )
gfp_settings = pmt.acquisitions.series.ChannelSettings(
    name='GFP', exposure=15, trigger = "Ext"
    )

# Stimulations:
reddmd_settings = pmt.acquisitions.series.StimulationsSettings(
    color='red', exposure = 60, intensity = 100, trigger = "Ext"
    )

# Acquisition series settings: (all using the same settings here)
series_settings = pmt.acquisitions.series.SeriesSettings(
    acquisition_mode = 'simultaneous',
    channels = [trans_settings, gfp_settings],
    stimulations = [reddmd_settings]
    )

# Add series
NUM_SERIES = 5
for s in range(NUM_SERIES):
    
    # Create single acquisition series:
    series = pmt.acquisitions.series.Series(
        series_number=s, platform=platform, settings = series_settings
        )
    
    # Get line of positions:
    series.positions_line()
    
    xp.series.append(series)

#%% Pre-process positions (ie acquire reference images & init processors):
xp.preprocess(
    control_server,
    delta_config = "C:/Users/System 6/Documents/config_mothermachine.json"
    )

# Assign control objectives to each cell / chamber:

#%% Run experiment:
xp.run()
