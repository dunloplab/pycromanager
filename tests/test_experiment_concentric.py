# -*- coding: utf-8 -*-
"""
Created on Thu Dec  9 14:18:03 2021

@author: jeanbaptiste
"""

# Not necessary once these are added to the python path permanently:
import os
import sys
# Append paths to delta-dev and deep cell control before import:
sys.path.append('C:/DeepLearning/DeLTA_dev')
sys.path.append('C:/Users/System 6/Documents/deepcellcontrol')
sys.path.append('C:/Users/System 6/Documents/pycromanager')
import time
import json

import matplotlib.pyplot as plt
import numpy as np

import pycromanager_tessie as pmt
from pycromanager_tessie.acquisitions.series import ChannelSettings, StimulationsSettings, SeriesSettings
from pycromanager_tessie.acquisitions.processors import SimpleImageSaver, MotherFeedback
import deepcellcontrol as dcc
import delta
delta.config.load_config(presets='mothermachine')
# delta.config.model_file_seg = "C:/Users/System 6/Downloads/unet_seg_moma_3lvl.hdf5"
delta_models=delta.utilities.loadmodels()


xp_folder = "E:/JBL/concentric/2022-02-07_concentric_18ms_part4/"
os.makedirs(xp_folder)


#%% Init platform:
platform = pmt.microscope.platform.Platform()

platform.neopixel.color(red=int(255/4))

# Reprod settings:
platform.set(
    "Andor sCMOS Camera",
    "Sensitivity/DynamicRange",
    "12-bit (low noise)"
    )

platform.set(
    "Spectra",
    "White_Level",
    50
    )
#%% Set up control server:
# TODO update parameters

# First, initialize controller object:
# controller = dcc.control.LSTMMPC(
#     model_file = 'C:/Users/System 6/Documents/deepcellcontrol/assets/models/lstm/batch_train_2021-10-15/2021-10-15_18-47-13/model.hdf5',
#     features=2,
#     latent_dim=64,
#     horizon=24,
#     past_steps=36,
#     strategy_optimizer=dcc.control.BinaryParticleSwarmOptimizer(
#         horizon=24, iterations=20, particles=20
#         )
#     )

controller = dcc.control.MLPMPC(
    model_file = 'C:/Users/System 6/Documents/deepcellcontrol/assets/models/mlp_scc/2022-01-23_19-30-13/model.hdf5',
    features=2,
    hidden_layers=10,
    horizon=24,
    past_steps=36,
    strategy_optimizer=dcc.control.BinaryParticleSwarmOptimizer(
        horizon=24, iterations=20, particles=20
        )
    )

#pass controller to control server:
control_server = dcc.server.Server(controller)
control_server.device = '/device:GPU:0' # What device to run computations
control_server.start() # Start server

# Create control objective (here all cells get the same objective):
# objectives = dcc.utilities.concentric_sines_objectives(
#     100, offset=1250, prop_speed=3, duration = 48*60
#     )/4095
# objectives = objectives[:,int(24*60/5):]

# # Generate objectives:
# rng = np.random.default_rng(1)
# shuffling = rng.choice(np.arange(objectives.shape[0]), size = objectives.shape[0], replace = False)

# deshuffling = shuffling.copy()
# for i, s in enumerate(shuffling):
#     deshuffling[s] = i

# objectives = objectives[shuffling, :]

og_folder = "E:/JBL/concentric/og/"
objectives = np.load(og_folder+"objectives.npy")
shuffling = np.load(og_folder+"shuffling.npy")
deshuffling = np.load(og_folder+"deshuffling.npy")



np.save(xp_folder+"objectives.npy", objectives)
np.save(xp_folder+"shuffling.npy", shuffling)
np.save(xp_folder+"deshuffling.npy", deshuffling)

# movie = np.reshape(objectives,(100,100,objectives.shape[1]))
# plt.imshow(movie[:,:,-1], vmin=0, vmax=1)
# plt.show()

ranked_folder = "E:/JBL/concentric/ranked/"
redo_obj = np.load(ranked_folder+"redo_obj.npy")
ranked = np.load(ranked_folder+"ranked.npy")

already_processed = 9315
print("!"*80+"\nGoing to run objectives starting from cell %d\n"%already_processed+"!"*80)


#%% First find channels etc then run DMD calibration:
platform.neopixel.shutter(True)
platform.dmd.calibrate(color = 'blue')
platform.neopixel.shutter(False)

#%% Init positions and series:

# Channels:
trans_settings = ChannelSettings(name = 'Trans', exposure = 10)
gfp_settings = ChannelSettings(name = 'GFP', exposure = 18)

# Stimulations:
reddmd_settings = StimulationsSettings(color='red', exposure = 60, intensity = 100)

# Acquisition series settings: (all using the same settings here)
series_settings = SeriesSettings(
    acquisition_mode = 'simultaneous',
    channels = [trans_settings, gfp_settings],
    stimulations = [reddmd_settings]
    )

series_list = []
total_pos = 0
for s in range(5):
    
    # Create single acquisition series:
    series = pmt.acquisitions.series.Series(
        series_number=s,
        platform=platform,
        settings = series_settings
        )
    
    # Get line of positions:
    series.positions_line()
    
    # Assign processors to each position:
    channel_names = [x.name for x in series.settings.channels]
    for pos, position in enumerate(series.positions):
        
        position.processors = {
            'filesaver' : SimpleImageSaver(
                xp_folder,
                position_nb = total_pos,
                channels = channel_names,
                watch_channels = tuple(range(len(channel_names)))
                ),
            'controller' : MotherFeedback(
                position_nb = total_pos,
                channels = channel_names,
                watch_channels = (0,), # Only watch first channel
                delta_models=delta_models, # delta U-Nets
                features = ('fluo1',), # Features to extract and feed into the controller
                normalization_fn = dcc.data.normalization, # make sure we use the same data normalization
                control_server = control_server, # Control server object
                device = '/device:GPU:0' # Device to run the delta analysis on
                )
            }
        
        total_pos +=1
    
    print(series)
    series_list.append(series)

for series in series_list:
    series.preprocess()

num_chambers = 0
for series in series_list:
    # Assign objective:
    for position in series.positions:
        processor = position.processors['controller']
        for mother in processor.mothers:
            movie_global = num_chambers + already_processed
            if movie_global >= 10_000:
                movie_global = ranked[-(movie_global-9999)]
            mother['objective'] = objectives[movie_global,:]
            mother['shuffling'] = shuffling[movie_global]
            mother['deshuffling'] = deshuffling[movie_global]
                
            num_chambers += 1
    
    series.check()
    
    for pos, position in enumerate(series.positions):
        position.processors['filesaver'].start()
        position.processors['controller'].start()

print("%d chambers"%num_chambers)

#%% 

objectives = []
shuffling = []
deshuffling = []
for series in series_list:
    # Assign objective:
    for position in series.positions:
        processor = position.processors['controller']
        for mother in processor.mothers:
                objectives += [mother["objective"]]
                shuffling += [mother["shuffling"]]
                deshuffling += [mother["deshuffling"]]
                
objectives = np.array(objectives)
shuffling = np.array(shuffling)
deshuffling = np.array(deshuffling)

np.save(xp_folder+"objectives_local.npy", objectives)
np.save(xp_folder+"shuffling_local.npy", shuffling)
np.save(xp_folder+"deshuffling_local.npy", deshuffling)

del objectives
del shuffling
del deshuffling

#%% to restart:

for series in series_list:
    series.reset()

#%% 
platform.studio.live().set_live_mode(False) # put as a method somewhere
stim_batch = 5

#TODO automatically create XP folder if doesn't exist
# Write all device properties to json file:
with open(xp_folder+"device_properties.json","w") as f:
    json.dump(platform.get_all_properties(), f, indent=4)

# Write series to txt file:
with open(xp_folder+"series.txt","w") as f:
    for series in series_list:
        f.write(str(series))
        f.write("\n"+"="*80+"\n\n\n\n")


#TODO set camera to 12bit low noise and Spectra to 50% (put that in settings?)
# Write somewhere that 12bit high cap seems to be faster

platform.neopixel.color(green=1)
frame_nb= 1

while 1:
    
    print("\n\n\n-------- Starting Frame %d"%frame_nb)
    t_start = time.perf_counter()
    for series in series_list:
        
        series.acquire()
        
        for i in range(0,len(series.positions),stim_batch):
            j = min(i+stim_batch,len(series.positions))
            series.stimulate(positions = list(range(i,j)))
    
    print("Frame %d"%frame_nb)
    print("Total acq time: %.1f"%(time.perf_counter()-t_start))
    frame_nb+=1
    while time.perf_counter()-t_start < 300:
        time.sleep(.1)

# Save images to disk during DMD loading? (ie make the call to DMD load non-blocking with a daemon or something?)

#%% lib issues:
import importlib
importlib.reload(pmt)
from pycromanager_tessie.acquisitions.processors import SimpleImageSaver, MotherFeedback

#%%
mothers = []
for series in series_list:
    mothers.append([])
    for pos in series.positions:
        mothers[-1].append([])
        for mother in pos.processors["controller"].mothers:
            mothers[-1][-1].append(mother)

import pickle
with open(xp_folder+"mothers.pkl","wb") as f:
    pickle.dump(mothers,f)

#%%


motarr = []
stimarr = []
for s in mothers:
    for p in s:
        for c in p:
                motarr += [c["fluo1"]]
                stimarr += [c["stims"]]
                
motarr = np.array(motarr)
stimarr = np.array(stimarr)


np.save(xp_folder+"fluo1.npy", motarr)
np.save(xp_folder+"stims.npy", stimarr)
