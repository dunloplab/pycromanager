# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 14:31:15 2022

@author: System 6
"""

import numpy as np

import pycromanager_tessie as pmt
from pycromanager_tessie.acquisitions import experiment

# Start platform:
platform = pmt.microscope.platform.Platform(load_synchronizer=True)

# Set LED ring
platform.neopixel.color(red=int(255/4))

# Reprod settings:
platform.set("Andor sCMOS Camera","Sensitivity/DynamicRange","12-bit (low noise)")
platform.set("Spectra","White_Level",50)
platform.set("DiaLamp","Intensity",800)

#%% Calibrate DMD:
platform.neopixel.shutter(True)
platform.dmd.calibrate(color = 'blue')
platform.neopixel.shutter(False)

#%% Experimental parameters:

# Init experiment object:
xp = experiment.MoMaOpenLoopExperiment(
    platform,
    delta_config = "C:/Users/System 6/Documents/config_mothermachine.json"
    )
xp.save_folder = "E:/JBL/dummy_tests/calibration14"
xp.stim_batches = None
xp.period = 300

s = 0
total_positions = 0

#%%

# Channels:
trans_settings = pmt.acquisitions.series.ChannelSettings(
    name='Trans', exposure=12, trigger = "Ext"
    )
gfp_settings = pmt.acquisitions.series.ChannelSettings(
    name='GFP', exposure=12, trigger = "Ext"
    )

# Stimulations:
reddmd_settings = pmt.acquisitions.series.StimulationsSettings(
    color='red', exposure=60, intensity = 100, trigger = "Ext"
    )

# Acquisition series settings: (all using the same settings here)
series_settings = pmt.acquisitions.series.SeriesSettings(
    acquisition_mode = 'simultaneous',
    channels = [trans_settings, gfp_settings],
    stimulations = [reddmd_settings]
    )


# Create single acquisition series:
series = pmt.acquisitions.series.Series(
    series_number=s, platform=platform, settings = series_settings
    )

# Get line of positions:
series.positions_line(index_offset=total_positions)

xp.series.append(series)

# TODO: keep track of this automatically?
total_positions+= len(series.positions)
s+=1

#%% Pre-process positions (ie acquire reference images & init processors):
# TODO display preprocessing for delta

xp.preprocess()

num_chambers = xp.total_chambers()

# TODO create a method or something for this:
ref1 = 36*[False] + (8*12*[True] + 8*12*[False])*2 + 12*[True]
ref2 = 36*[False] + (8*12*[False] + 8*12*[True])*2 + 12*[False]
stims = np.zeros((num_chambers,len(ref1)),dtype=bool)
# Assign 2*100 "reference" cells to go through pre-set cycles:
references = np.random.choice(num_chambers, num_chambers, replace = False)
stims[references[:int(num_chambers/2)],:] = ref1
stims[references[int(num_chambers/2):],:] = ref2

xp.assign_stimulations(stims.astype(int))


#%% Run experiment:
platform.neopixel.color(green=1)
# xp.run()
xp.start()

# TODO end of XP saving etc...
#%%
import pickle
import numpy as np
import matplotlib.pyplot as plt
import os

os.makedirs(xp.save_folder+"delta_positions/", exist_ok = True)

mothers = []
rois = []
drift = []
z_history = []
for series in xp.series:
    mothers.append([])
    rois.append([])
    drift.append([])
    z_history.append([])
    for pos in series.positions:
        mothers[-1].append([])
        rois[-1].append([])
        controller = pos.processors["controller"]
        drift[-1].append(controller.drift)
        z_history[-1].append(pos.z_history)
        
        for mother in controller.mothers:
            mothers[-1][-1].append(mother)
        for roi in controller.delta_position.rois:
            rois[-1][-1].append(dict(roi.box.items()))
        
        # controller.delta_position.save(
        #     xp.save_folder+"delta_positions/Pos%06d"%pos.number,
        #     save_format=("pickle")
        #     )
        
        

# with open(xp.save_folder+"mothers.pkl","wb") as f:
#     pickle.dump(mothers,f)

# with open(xp.save_folder+"roi_boxes.pkl","wb") as f:
#     pickle.dump(rois,f)
    
# with open(xp.save_folder+"drift.pkl","wb") as f:
#     pickle.dump(drift,f)

# with open(xp.save_folder+"z_history.pkl","wb") as f:
#     pickle.dump(z_history,f)



#%%

for s_i, s in enumerate(mothers):
        
    fluo1 = []
    fluo2 = []
    # s = mothers[0]
    # for s in mothers:
    for p in s:
        for mother in p:
            if all(mother["stims"]==1):
                fluo1.append(mother["fluo1"])
            elif all(mother["stims"]==0):
                fluo2.append(mother["fluo1"])
    
    # for f in fluo1:
    #     if len(f) > 145:
    #         del f[145:]
    
    # for f in fluo2:
    #     if len(f) > 145:
    #         del f[145:]
    
    def plotq(fluo,q = .5, color="b"):
        
        x = np.arange(0,len(fluo[0]),1)/12
        plt.plot(x,np.median(fluo,axis=0), color=color)
        plt.fill_between(
            x,
            np.quantile(fluo,.5-q/2,axis=0),
            np.quantile(fluo,.5+q/2,axis=0),
            color=color,
            alpha=.2,
            )
    
    plotq(fluo1)
    plotq(fluo2,color="orange")
    plt.xlabel("time (hours)")
    plt.ylabel("GFP (a.u.)")
    plt.ylim([0, 4_095])
    plt.grid(which="both", axis="both")
    plt.title(f"series {s_i}")
    # plt.title(" ".join([str(p.position)]))
    
    # plt.plot([7, 7],[0,25_000],"--k")
    # plt.plot([17.7, 17.7],[0,25_000],"--k")
    # plt.plot([22.83, 22.83],[0,25_000],"--k")
    
    # fluo = np.array(fluo1+fluo2)
    # plt.hist(fluo[:,0].flatten(), bins=20, range=(150,1000))
    # plt.ylim((0,210))
    
    plt.show()

#%%

for series in xp.series:
    for pos in series.positions:
        plt.plot(
            (np.array(pos.z_history["time"]) - pos.z_history["time"][1])/3600,
            pos.z_history["value"]
            )
        plt.xlabel("time (h)")
        plt.ylabel("Z value (um)")
        plt.title(pos.number)
        plt.savefig(xp.save_folder+"z_history/pos%06d.png"%pos.number)
        plt.show()
        
        drift = pos.processors["controller"].drift
        drift = np.squeeze(np.array(drift))
        plt.plot(
            np.arange(0,drift.shape[0])/12,
            drift
            )
        plt.xlabel("time (h)")
        plt.ylabel("drift (pixels)")
        plt.title(pos.number)
        plt.legend(("X","Y"))
        plt.savefig(xp.save_folder+"drift_history/pos%06d.png"%pos.number)
        plt.show()

#%%        
import cv2

framenb = len(xp._acq_timepoints)
for series in xp.series:
    for pos in series.positions:
        c = pos.processors["controller"]
        R = c._dmd_images["red"]
        R = platform.dmd.estimate_projection(R)
        R = (R.astype(np.float32)/255)*0.2
        I = cv2.imread(xp.save_folder+"pos%04d/chan01_frame%06d.tif"%(pos.number+1,framenb), cv2.IMREAD_ANYDEPTH).astype(np.float32)
        I = (I-np.min(I))/np.ptp(I)
        RGB = np.stack((I+R, I, I),axis=2)
        RGB = np.fliplr(RGB)
        plt.imshow(RGB)
        stims = ""
        for x in c.mothers:
            stims+=str(x["stims"][framenb-1])
        plt.title(f"Position {pos.number}\n{stims}")
        plt.show()

#%%

for series in xp.series:
    for channel in series.settings.channels:
        if channel.trigger == "Int":
            channel.trigger = "Ext"
        else:
            channel.trigger = "Int"
    
    for stim in series.settings.stimulations:
        if stim.trigger == "Int":
            stim.trigger = "Ext"
        else:
            stim.trigger = "Int"

#%%

import time
while len(xp._acq_timepoints) < 192:
    time.sleep(1)
time.sleep(10)
while xp._running_timepoint:
    time.sleep(1)

# xp.series = xp.series[::-1]
# trans_settings.exposure = 60
for series in xp.series:
    series.settings.channels[1].exposure = 6
# reddmd_settings.exposure = 120
