Micro-Manager & pycromanager
============================

`Micromanager <https://micro-manager.org>`_ is an open source microscope 
control application. 
It is written mostly in Java, with most of the device adapters (kind of like
drivers) written in C++. It features a broad collection of adapters that make 
it usable on many setups (Nikon, Zeiss etc...). Because its core is written
in Java, it was easy to interface with Matlab (Matlab is Java under the hood).
It provides a so-called "core" API that can be used to execute all the
operations required to conducted timelapse experiments with DMD
optogenetic stimulations, such as moving the XY platform, snapping images, or
loading images into the DMD.

Unfortunately Matlab is fundamentally single-threaded, making it hard to run
operations asynchronously (eg acquiring images while running image analysis in
parallel) which severely limits throughput.
In addition it makes it hard to interface with DeLTA which is written in python.

Recently the people behind micromanager published a python-java
bridge called `pycromanager <https://github.com/micro-manager/pycro-manager>`_
that makes it possible to interface python with micromanager.

``pycromanager_tessie`` relies mostly on this micromanager-pycromanager stack to 
drive the platform. A couple custom arduino scripts are 
additionally used to control the Neopixel LED ring and to conduct hardware 
synchronization. See :ref:`Arduinos <arduinos>` and 
:doc:`Hardware Synchronization <hardware_sync>`

The reference to the micromanager core API that is at the, well, core of
``pycromanager_tessie`` can be found here:
`MMCore API <https://valelab4.ucsf.edu/~MM/doc/MMCore/html/class_c_m_m_core.html>`_

Finally, I kept a log of the installation process 
`here <https://docs.google.com/document/d/1FJLSwHWrxWikXzEVT3H5vRwgQVNQVqu4n6kiU_kys0U>`_.
It is not completely up to date, but should help if you want to reinstall
everything or want to set up another microscope.

Device properties
---------------------

Below is a list of the devices connected to micromanager as well as 
descriptions of their properties. These can be found in the property browser
in the MM interface under Devices > Devices Properties Browser.

.. literalinclude:: ./devices_descriptions.txt