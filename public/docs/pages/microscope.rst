microscope sub-package
============================

The microscope sub-package under ``pycromanager_tessie/microscope`` implements 
the "hardware" side of the library.

The main module within it is ``platform.py``, which implements the ``Platform``
class that coordinates most operations and handles communication with 
micromanager via pycromanager's java bridge.

The ``DMD`` class in ``dmd.py`` focusses on DMD operations, which it performs 
via the ``Platform`` class, as well as implements projection correction methods
(see below). 

The ``NeoPixel`` class in ``neopixel.py`` simply handles communication with the
NeoPixel LED ring's arduino to set its color and intensity, and can be 
controlled by ``Platform`` to be shut on and off when acquiring images. 

Finally, the ``Synchronizer`` class in ``synchronizer.py`` is a bit different:
When loaded and used, if the relevant equipment has been set to 
hardware/external triggering mode the ``Synchronizer`` can supersede
``Platform`` and ``DMD`` to coordinate operations. 

A script demonstrating the main functionalities of the microscope sub-package
can be found under ``scripts/microscope_demo.py``. It does not cover hardware
synchronization.


.. image:: ../images/Microscope_package.png
    :align: center


platform module
----------------

This module contains the definition of the ``Platform`` class, which is the core
of the microscope package. It coordinates hardware to perform high-level
operations between the microscope, the DMD, and the Arduinos.

Once instantiated, the ``Platform`` object will instantiate the DMD object, the
NeoPixel object, and the Synchronizer object (if connected and specified). The 
``acquisitions`` mostly uses this ``Platform`` object to direct microscope 
operations.

The ``Platform`` object itself implements all imaging-related operations that are
available through pygromanager: Snap images, change imaging channels, 
exposure time, XY position, autofocussing etc... DMD-related operations 
however are handled via the ``DMD`` object. It is also used to retrieve 
information about device properties and contains a few methods to interface
with micromanager's GUI (live imaging window and positions list).

dmd module
-----------

This module implements primarily the ``DMD`` class, and a couple DMD-related
utility functions. It is normally instantiated by the ``Platform`` object, and
is an attribute of it but it is mostly independent of it. 

It contains a few methods to (re-)calibrate the DMD after each bootup: It 
basically just shines a sequence of dots in pre-determined spots and detects 
where they are projected via the camera. This allows us to compute a 
DMD <-> Camera homography matrix that can then be used to transform coordinates
in one "plane" to the other one.

Once this calibration is performed, we can infer what image to load into 
the DMD to obtain the projection that we want onto the field of view of the
camera (and thus, target specific cells or chambers in the image).

neopixel module
----------------

This module simply implements the ``NeoPixel`` class with a serial connection to
the arduino that controls the NeoPixel LED ring. It can set its color and
intensity. Be aware though that if the "firmware" under 
``arduino_code/neopixels_python`` is not loaded into the arduino (you can just
use the simple Arduino IDE to do that) the ``NeoPixel`` object will successfully
connect to it, but the arduino will not respond to commands. This typically
happens when someone uses the LED ring via Matlab.

synchronizer module
----------------------

The ``Synchronizer`` class in this module features a serial connection to the 
hardware synchronization arduino, and handles coordination with the `Platform`.
Through the platform it sets relevant equipments in hardware synchronization
mode, and sends commands to the arduino to trigger certain hardware sequences.

.. note::

    Unfortunately, when it comes to Trans imaging, the lamp can not be triggered
    by the arduino, which is why hardware acquisition sequences are actually 
    called via the `Platform.hw_acquire()` method. For consistency the
    corresponding ``.hw_frame()`` & ``.hw_shine_frame()`` methods have been
    added to the `DMD` class.

If you are using it, make sure that the firmware from 
``arduino_code/sync_arduino`` is loaded in the arduino (you can just use the 
standard arduino IDE for that), and that the arduino is connected properly both
to the computer and to the BNC cables for TTL triggering. For more details on
this see :ref:`synchronization arduino <arduinos>` and 
:doc:`hardware synchronization <hardware_sync>`.
