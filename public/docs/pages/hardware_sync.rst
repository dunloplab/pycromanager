Hardware synchronization
=========================

The reason
for using hardware triggering is that, because the signals that are being sent
via computer-based triggering have to go through so many application layers and
communication hubs, they can accumulate significant delays or even get caught 
in some short but annoyingly random i/o hang-ups. With hardware triggering,
because this is all handled by relatively simple code on an arduino and direct
TTL signals carried by coaxial cables, coordination latencies are greatly
reduced and hang-ups do not happen, allowing us to greatly increase throughput.