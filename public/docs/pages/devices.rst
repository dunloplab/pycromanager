Devices
============

Here is a general description of the devices on the platform, and in general 
how Tessie works.

Platform description
---------------------

Here are the different devices on Tessie as well as a description of the
main controls:

.. image:: ../images/tessie.png
    :align: center

.. image:: ../images/dmd.png
    :align: center

.. image:: ../images/fluo_fiber.png
    :align: center

.. image:: ../images/condenser.png
    :align: center

.. image:: ../images/front.png
    :align: center

.. image:: ../images/dialamp_side.png
    :align: center

.. image:: ../images/turret_side.png
    :align: center

.. image:: ../images/joystick.png
    :align: center

Startup
--------

To start Tessie follow these steps:

#. Make sure everything is turned off, even the power strip.
#. If using the :ref:`hardware synchronization arduino <arduinos>`, connect all the BNCs, the 
   USB to the PC, and the power supply to the power strip.
#. Turn on Tessie's computer, wait for it to boot completely (optional: Open 
   the device manager to keep track of devices connecting to the computer)
#. Turn on the power strip, the Ti2 control box, and the Ti2 chassis in rapid
   succession. You must not wait too long between turning all of these on 
   because otherwise they may not initialize properly and you might run into
   communication issues.
#. Wait for the Ti2 to finish initializing. If it beeps an error, just turn the
   chassis, box, and power strip off, wait a couple minutes and then turn them
   back on. If it keeps erroring out we have a problem.
#. Turn on the camera
#. Turn on the XCITE XLED1 (DMD lamp)
#. Turn on the DMD
#. Wait at least 5 minutes, then start micro-manager 2.0-gamma and select the
   "WithDMD" config file, wait for the main command GUI to pop up.
#. Open the anaconda powershell prompt::
   
       (base)$ conda activate pycromanager_env
       (pycromanager_env)$ spyder
   
#. In spyder, start your script or simply run::
   
       import pycromanager_tessie as pmt
       platform = pmt.microscope.platform.Platform()
   
   to connect to micro-manager


.. note::
    Be careful not to touch the button on the front of the SOLA lamp. This 
    is a shutter button not an on/off button. The lamp turns on with the 
    power strip.


About the Ti-LAPP
-----------------

Nikon's Ti-LAPP system is connected to the back port of the microscope. It is 
supposed to allow us to select between light coming from the DMD or the 
fluorescent lamp to shine onto our sample. Unfortunately, Nikon does not
share the information necessary for micro-manager to be able to interface with
it. To circumvent this issue, we replaced the 100% mirror that was inside it
by a 50% reflective / 50% passing beamsplitter.

However this mirror will only let both Fluorescence and DMD light go into the
back port in one of its two positions. In the other position the fluorescence 
lamp will not go to the microscope. Of course, the Ti-LAPP boots up in the 
wrong position.

To make switching possible, we mapped the "1" button on Tessie's joystick to 
switch between these two positions. So remember upon boot up to press the "1" 
button on the joystick, and if you are not seeing any fluorescence signal this
might be the cause.

Start up position::

	DMD * * * * * * * *\
	                   *
	                   
	                   *
	                   
	                   *
	 50/50 mirror--->   |+ + + + + + + + + Fluo lamp
	                   *
	                   
	                   *
	                   
	                   *
	                   
	             to microscope


Position after pressing "1" on joystick::

	DMD * * * * * * * *\
	                   *
	                   
	                   *
	                   
	                   *
	 50/50 mirror--->  /+ + + + + + + + + Fluo lamp
	                   *
	                   +
	                   *
	                   +
	                   *
	                   +
	             to microscope


Fluorescence filter cubes
--------------------------

Here are the filter cubes that are currently (2022) set up in the filters turret:

* Position 1: Beamsplitter (for Trans and DMD) - 
  `Chroma 21001 80/20 Beamsplitter <https://www.chroma.com/products/parts/80-20-beamsplitter>`_
* Position 2: CFP cube - 
  `Chroma 49001 ET eCFP <https://www.chroma.com/products/sets/49001-et-ecfp>`_
* Position 3: GFP cube - 
  `Chroma 49002 ET eGFP <https://www.chroma.com/products/sets/49002-et-egfp-fitc-cy2>`_
* Position 4: YFP cube - 
  `Chroma 49003 ET eYFP <https://www.chroma.com/products/sets/49003-et-eyfp>`_
* Position 5: RFP cube - 
  `Chroma 49008 ET mCherry/TexasRed <https://www.chroma.com/products/sets/49008-et-mcherry-texas-red>`_
* Position 6: DAPI/BFP cube - 
  `Chroma 49021 ET eBFP2/Coumarin/Att. DAPI <https://www.chroma.com/products/sets/49021-et-ebfp2-coumarin-attenuated-dapi>`_

.. _arduinos:

Arduinos
---------

There are 2 arduino unos that can be used to extend Tessie's capabilities:

* One controls the Neopixel LED ring and is typically always connected 
  to the computer. Currently it connects to the COM6 port. The code that allows
  it to interface with matlab and with python are different, if it does not 
  work or you know that the code has been changed, reload the code in
  ``arduino_code/neopixels_python`` in this repository.
* The other one is for hardware synchronization. Typically it is removed
  from the platform when not in use to avoid potential triggering conflicts
  (although I'm not sure if there are any actual potential conflicts.)
  Currently, it connects to the COM9 port.
  For more information, see :doc:`hardware synchronization <hardware_sync>`

.. note::
    These are not connected to micro-manager / pycromanager at all. The 
    communication with them happens via the ``pyserial`` library, and I wrote
    custom python classes under ``pmt.microscope.neopixel.NeoPixel`` and
    ``pmt.microscope.synchronizer.Synchronizer`` to implement the relevant 
    methods.
