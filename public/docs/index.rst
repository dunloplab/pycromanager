.. pycromanager_tessie documentation master file, created by
   sphinx-quickstart on Tue Apr 12 16:06:05 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pycromanager_tessie's documentation!
===============================================

``pycromanager_tessie`` is a python package that uses the micromanager-pycromanager
stack to control Tessie, and interfaces with the 
`DeLTA <https://gitlab.com/dunloplab/delta>`_ and 
`deepcellcontrol <https://gitlab.com/dunloplab/deepcellcontrol>`_ packages on 
Tessie's computer to conduct high-throughput feedback experiments.

It is divided in 2 main sub-packages: 

* | The :doc:`microscope sub-package <pages/microscope>`, that handles the hardware
    operations, and for the most part simply repackages pycromanager commands
    to make them more user-friendly and use default settings that seem to work
    best.
* | The :doc:`acquisitions sub-package <pages/acquisitions>`, that manages 
    experiments logic like order of acquisitions, timing, interfacing with
    DeLTA and DCC etc...


.. warning::
    This documentation is a work in progress


Contents
----------

.. toctree::
    :maxdepth: 2
    
    pages/devices
    pages/micromanager
    pages/microscope
    pages/acquisitions


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

About this documentation
-------------------------

This documentation is built using 
`sphinx <https://www.sphinx-doc.org/en/master/>`_, with the numpydoc, autodoc, 
and autosummary extensions. The source RST pages are inside ``public/docs`` and
``public/docs/pages``.

To build simply run the following shell command at the root of the repository, 
in the pycromanager environement (+ sphinx packages)::

    sphinx-build -b html public/docs/ public/


When the changes are pushed to gitlab, the CI/CD pipeline will publish the new
generated html pages (see ``.gitlab-ci.yml``)

Note: The modifications might take a while to appear online,
for caching reasons.