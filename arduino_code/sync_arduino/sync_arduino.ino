// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

// Which pins connect to which:
#define CAM_TRIGGER_PIN   2
#define CAM_AUX_PIN       3

# define DMD_FRAME        7

# define RED_DMD_PIN      10
# define GREEN_DMD_PIN      11
# define BLUE_DMD_PIN      12

# define FLUO_PIN      13

# define RING_SHUTTER_PIN   5

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.

byte command[3];

bool cam_busy;
bool cam_busy_old;

bool dmd_busy;
bool dmd_busy_old;

unsigned long countdown_target;

union b2toi16 {
  byte command[2];
  uint16_t integer;
};

b2toi16 exposure;

void setup() {
  
  // initialize serial:
  Serial.begin(115200);

  pinMode(CAM_TRIGGER_PIN, OUTPUT);
  pinMode(CAM_AUX_PIN, INPUT);

  pinMode(DMD_FRAME, OUTPUT);
  
  pinMode(RED_DMD_PIN, OUTPUT);
  pinMode(GREEN_DMD_PIN, OUTPUT);
  pinMode(BLUE_DMD_PIN, OUTPUT);

  pinMode(FLUO_PIN, OUTPUT);
  digitalWrite(FLUO_PIN, HIGH); // SHUTTER: Turns off when low, but spectra state must be set to 1 in MM (basically an AND gate)

  pinMode(RING_SHUTTER_PIN, OUTPUT); // SHUTTER: Turns off when set to LOW.
  digitalWrite(RING_SHUTTER_PIN, HIGH);
}

void loop() {
  
  // Update memory flags:
  cam_busy_old = cam_busy;
  dmd_busy_old = dmd_busy;

  while (Serial.available() >= 3) {

    Serial.readBytes(command, 3);
    //incomingByte = Serial.read() - '0'; // remove the - '0'
    //Serial.println(incomingByte);

    //Serial.println((unsigned int) command[0]);
    
    switch ((unsigned int)command[0]){
      case 0: // Just trigger the camera
        cam_acq();
        break;
      case 1: // Turn on fluo lamp, then trigger camera:
        digitalWrite(FLUO_PIN, HIGH);
        cam_acq();
        digitalWrite(FLUO_PIN, LOW);
        break;

      case 10:  // just trigger dmd frame change
        dmd_frame_trigger();
        break;
      case 11: // activate red dmd lamp, then trigger next frame
        digitalWrite(RED_DMD_PIN, HIGH);
        dmd_countdown();
        digitalWrite(RED_DMD_PIN, LOW);
        dmd_frame_trigger();
        Serial.write((byte)2);
        break;
      case 12: // activate green dmd lamp, then trigger next frame
        digitalWrite(GREEN_DMD_PIN, HIGH);
        dmd_countdown();
        digitalWrite(GREEN_DMD_PIN, LOW);
        dmd_frame_trigger();
        Serial.write((byte)2);
        break;
      case 13: // activate blue dmd lamp, then trigger next frame
        digitalWrite(BLUE_DMD_PIN, HIGH);
        dmd_countdown();
        digitalWrite(BLUE_DMD_PIN, LOW);
        dmd_frame_trigger();
        Serial.write((byte)2);
        break;

      case 100: // Fluo lamp trigger pin set to LOW
        digitalWrite(FLUO_PIN, LOW);
        break;

      case 101: // Fluo lamp trigger pin set to HIGH (default)
        digitalWrite(FLUO_PIN, HIGH);
        break;
        
      default:
        Serial.write((byte) 255);
        break;
    }
  }
}

void cam_acq() {

  // Shut off ring:
  digitalWrite(RING_SHUTTER_PIN, LOW);
  delay(3); // Just to be safe

  // Trigger camera:
  digitalWrite(CAM_TRIGGER_PIN, HIGH);
  delayMicroseconds(50);
  digitalWrite(CAM_TRIGGER_PIN, LOW);

  // Wait for camera to stop being busy:
  while (digitalRead(CAM_AUX_PIN)){
    delayMicroseconds(50);
  }

  // Turn ring back on:
  digitalWrite(RING_SHUTTER_PIN, HIGH);

  // Signal that camera frame is ready:
  Serial.write((byte) 1);
}

void dmd_frame_trigger() {
  digitalWrite(DMD_FRAME, HIGH);
  delayMicroseconds(1);
  digitalWrite(DMD_FRAME, LOW);
}

void dmd_countdown() {
  exposure.command[0] = command[1];
  exposure.command[1] = command[2];
  delay(exposure.integer);
}

//void start_countdown() {
//  exposure.command[0] = command[1];
//  exposure.command[1] = command[2];
//  countdown_target = millis() + exposure.integer;
//}
//
//bool check_countdown() {
//  return countdown_target > millis(); // True if busy (ie countdown still running)
//}
