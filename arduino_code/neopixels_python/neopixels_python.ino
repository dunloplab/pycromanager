// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN            6
#define SHUTTER_PIN    13

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      24

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int delayval = 500; // delay for half a second
byte colors[3];

unsigned int red = 0;
unsigned int green = 0;
unsigned int blue = 0;

bool shutter = false;

void setup() {
  // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
#if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
#endif
  // End of trinket special code
  
  // initialize serial:
  Serial.begin(115200);
  
  pixels.begin(); // This initializes the NeoPixel library.

  set_ring(red,green,blue);

  pinMode(SHUTTER_PIN, INPUT_PULLUP); // Using INPUT_PULLUP so it is always ON if nothing is connected
}

void loop() {

  while (Serial.available() >= 3) {

    Serial.readBytes(colors, 3);

    red = (unsigned int)colors[0];
    green = (unsigned int)colors[1];
    blue = (unsigned int)colors[2];
    
    // For a set of NeoPixels the first NeoPixel is 0, second is 1, all the way up to the count of pixels minus one.
    if (!shutter) {
      set_ring(red,green,blue);
    }
  }

  // Shutter rising front:
  if (!digitalRead(SHUTTER_PIN) && !shutter) {
    shutter = true;
    set_ring(0,0,0);
  }

  // Shutter falling front:
  if (digitalRead(SHUTTER_PIN) && shutter) {
    shutter = false;
    set_ring(red,green,blue);
  }
  
}

void set_ring(unsigned int r, unsigned int g, unsigned int b) {
  for(int i=0;i<NUMPIXELS;i++){
        // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
        pixels.setPixelColor(i, pixels.Color(r,g,b)); // Moderately bright green color.
      }
      pixels.show(); // This sends the updated pixel color to the hardware.
}
