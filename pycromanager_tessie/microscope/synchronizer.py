    # -*- coding: utf-8 -*-
"""
Created on Wed Mar  2 18:15:32 2022

@author: System 6
"""

import time
import warnings
import serial

# TODO: Add threading inheritance and monitor serial feedback?

class Synchronizer:
    '''
    Class to control the hardware synchronization arduino.
    '''

    def __init__(self, platform, port='COM6', baudrate=115200, timeout=.1):
        '''
        Instanciate

        Parameters
        ----------
        platform: pmt.microscope.platform
            The general platform object
        port : str, optional
            COM port of the controlling arduino. The default is 'COM9'.
        baudrate : int, optional
            Communication baud rate. Must match the value set in neopixels_python.ino.
            The default is 115200.
        timeout : float, optional
            Timeout delay, in seconds.
            The default is .1.

        Returns
        -------
        None.

        '''

        self.platform = platform
        "The microscope control pmt.microscope.platform object"
        self.serial = serial.Serial(port=port, baudrate=baudrate, timeout=timeout)
        "pyserial object to communicate with the arduino"
        time.sleep(3) # Needs a few seconds to finish intializing
        self._camera = False
        "Camera ready status"
        self._dmd = False
        "DMD ready status"

        # TODO: add handshake to make sure the arduino program is loaded
        print("%s, %d baud rate, %f s timeout"%(port, baudrate, timeout))

    # TODO:
    def set_camera(self):
        """
        Set camera to be triggerable by arduino

        Returns
        -------
        None.

        """

        if not self.platform.get("Andor sCMOS Camera","TriggerMode") == "External":
            self.platform.set("Andor sCMOS Camera","TriggerMode", "External")

        if not self.platform.get("Andor sCMOS Camera","AuxiliaryOutSource (TTL I/O)") == "FireAny":
            self.platform.set("Andor sCMOS Camera","AuxiliaryOutSource (TTL I/O)", "FireAny")

        self._camera = True

    def set_fluo_lamp(self):
        """
        Set Fluo lamp to be triggerable by arduino

        Returns
        -------
        None.

        """

        # Reminder: the spectra trigger system is an AND gate between the
        # external trigger input coming from the BNC, and the value set
        # by micromanager under Spectra-State

        # Set arduino trigger off (for now):
        exposure = 0 # Dummy value, not actually used by arduino
        commandbyte = bytes([100]) # Shutter OFF
        self.serial.write(commandbyte+exposure.to_bytes(2,"little"))

        # Set spectra state on while we are in external trigger mode:
        self.platform.set("Spectra","State", 1)

    def set_dmd(self):
        """
        Set DMD and DMD lamp to be triggerable by arduino
        This needs to be set BEFORE loading images into arduino

        Returns
        -------
        None.

        """

        self.platform.set("LED4_red","L.12 Pulse Mode (0:DC 1:Int 2:Ext 3:Glbl)",2)
        self.platform.set("LED3_green","L.12 Pulse Mode (0:DC 1:Int 2:Ext 3:Glbl)",2)
        self.platform.set("LED1_blue","L.12 Pulse Mode (0:DC 1:Int 2:Ext 3:Glbl)",2)

        self.platform.set("MightexPolygon", "TriggerType", 2)

        self._dmd = True

    def unset_fluo_lamp(self):
        """
        Make fluo lamp computer-controlled again

        Returns
        -------
        None.

        """

        # Set spectra state back to off:
        self.platform.set("Spectra","State", 0)

        # Set arduino trigger back on:
        exposure = 0 # Dummy value, not actually used by arduino
        commandbyte = bytes([101]) # Shutter OFF
        self.serial.write(commandbyte+exposure.to_bytes(2,"little"))

    def unset_camera(self):
        """
        Make camera computer-controlled again

        Returns
        -------
        None.

        """

        if not self.platform.get("Andor sCMOS Camera","TriggerMode") == "Internal (Recommended for fast acquisitions)":
            self.platform.set("Andor sCMOS Camera","TriggerMode", "Internal (Recommended for fast acquisitions)")
        self._camera = False

    def unset_dmd(self):
        """
        Make DMD and DMD lamp computer-controlled again

        Returns
        -------
        None.

        """

        self.platform.set("LED4_red","L.12 Pulse Mode (0:DC 1:Int 2:Ext 3:Glbl)",0)
        self.platform.set("LED3_green","L.12 Pulse Mode (0:DC 1:Int 2:Ext 3:Glbl)",0)
        self.platform.set("LED1_blue","L.12 Pulse Mode (0:DC 1:Int 2:Ext 3:Glbl)",0)

        self.platform.set("MightexPolygon", "TriggerType", 0)

        self._dmd = False


    def snap(self, channel="Trans"):
        """
        Trigger camera acquisition, with shutter sync

        NOTE: If trans snaps are giving you weird images where you only have
        the center of the image, it is probably because you have camera-ready
        acknowldgements queued up in the serial. To solve this issue, run
        the .clear() method

        Parameters
        ----------
        channel : str, optional
            The channel for the acquisition (THE CHANNEL WILL NOT BE SET VIA
            MICROMANAGER, THIS IS ONLY TO INDICATE WHICH LAMP TO TRIGGER TO
            THE ARDUINO)
            The default is "Trans".

        Returns
        -------
        None.

        """

        exposure = 0 # Dummy value, not actually used by arduino

        # Unfortunately we can not trigger the trans lamp, we need to do
        # this through MM:
        if channel.lower() == "trans":
            self.platform.set('DiaLamp','State','1')
            # time.sleep(.300)
            commandbyte = bytes([0])
        elif channel.lower() in ("rfp","yfp","gfp","dapi"):
            commandbyte = bytes([1])
        else:
            raise ValueError(f"Channel {channel} not recognized")

        # Send bytes to arduino (little-endian, 16-bit for exposure)
        self._camera = False
        self.serial.write(commandbyte+exposure.to_bytes(2,"little"))

        # Unfortunately for trans we have to wait to turn off the lamp:
        if channel.lower() == "trans":
            while not self.camera_ready():
                time.sleep(.001)
            self.platform.set('DiaLamp','State','0')

    def stimulation_sequence_start(self):
        """
        Trigger first frame in DMD sequence, don't shine light

        Returns
        -------
        None.

        """

        exposure = 0
        commandbyte = bytes([10])

        self.serial.write(commandbyte+exposure.to_bytes(2,"little"))


    def stimulation_sequence_step(self, color, exposure):
        """
        Trigger DMD lamp, then trigger next DMD frame in the sequence

        Parameters
        ----------
        color : str
            Color for the stimulation, Can be "red", "green", or "blue".
        exposure : int
            exposure, in ms. Valid range is 0-65535

        Returns
        -------
        None.

        """

        if color=="red":
            commandbyte = bytes([11])
        elif color=="green":
            commandbyte = bytes([12])
        elif color=="blue":
            commandbyte = bytes([13])
        else:
            raise ValueError(f"Unrecognized color {color}")

        self._dmd = False
        cmd = commandbyte+exposure.to_bytes(2,"little")
        self.serial.write(cmd)


    def camera_ready(self):
        """
        Check if camera has finished acquiring

        Returns
        -------
        bool
            Camera ready flag.

        """
        self._monitor()
        return self._camera



    def dmd_ready(self):
        """
        Check if DMD has finished shining

        Returns
        -------
        bool
            DMD ready flag.

        """
        self._monitor()
        return self._dmd

    def _monitor(self):
        """
        Monitor the serial incoming stream for equipment status bytes and
        update the relevant flags

        Returns
        -------
        None.

        """

        while self.serial.in_waiting  > 0:
            incoming_byte = self.serial.read()

            if incoming_byte == bytes([1]):
                self._camera = True

            elif incoming_byte == bytes([2]):
                self._dmd = True

    def clear(self):
        """
        Retrieve all bytes from input serial

        Returns
        -------
        List of bytes

        """

        queued_bytes = []
        while self.serial.in_waiting  > 0:
            queued_bytes += [self.serial.read()]

        return queued_bytes


    def close(self):
        '''
        Close serial connection

        Returns
        -------
        None.

        '''
        self.serial.close()
