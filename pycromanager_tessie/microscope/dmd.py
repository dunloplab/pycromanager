# -*- coding: utf-8 -*-
"""
This module implements primarily the `DMD` object, and a couple DMD-related
utility functions.

"""
import os
import time
import threading

import cv2
import numpy as np
import matplotlib.pyplot as plt

class DMD():

    def __init__(self, platform):
        self.platform = platform
        "Ref to main platform object"
        self.mmc = platform.mmc
        "Ref to MMCore"
        self.name = self.mmc.get_slm_device()
        "Name of the DMD in MM configuration"
        self.LEDs = dict(red='LED4_red',green='LED3_green', blue='LED1_blue')
        "Dictionary of the names of the red, green, and blue in MM configuration"
        self.shape = (
            self.mmc.get_slm_height(self.name),
            self.mmc.get_slm_width(self.name)
            )
        "Shape of the images that can be loaded in the DMD"
        self.homography = None
        "Homography matrix, as computed by the calibration method"
        self.homography_inverse = None
        "Inverted homography matrix, as computed by the calibration method"
        print(self.name)
        print(self.LEDs)
        print(self.shape)

    def set_image(self, image):
        """
        Set image on the polygon

        Parameters
        ----------
        image : 2D array of uint8
            The image to shine on the DMD. The dimensions must match those of
            the .shape attribute

        Returns
        -------
        None.

        """
        self.mmc.set_slm_image(self.name,image)


    def focus(self, color='blue'):
        """
        Image focussing routine

        Parameters
        ----------
        color : str, optional
            The color to use for the focussing.
            The default is 'blue'.

        Returns
        -------
        None.

        """

        # Make sure the ti-lapp mirror is in position (press button 1 on joystick):
        _ = input("Please first make sure the Ti-LAPP mirror is in position...")

        # Set DMD image:
        self.set_image(self.calib_image())

        # Turn on:
        self.set_channel(color, exposure=1, intensity=5, trigger='DC')
        self.mmc.set_property(
            self.LEDs[color.lower()], 'L.09 On/Off State (1=On 0=Off)', 1
            )

        # Wait for focus:
        _ = input("Please focus then press enter...")


    def calibrate(self, color='blue'):
        """
        Calibration routine. Will set the homography attributes of the object.

        Parameters
        ----------
        color : str, optional
            The color to use for the focussing.
            The default is 'blue'.

        Raises
        ------
        RuntimeError
            If not enough calibration were acquired. This is probably an
            imaging issue.

        Returns
        -------
        None.

        """

        print("\n################ DMD Calibration #################\n")

        # If live is on, turn it off:
        self.platform.studio.live().set_live_mode(False)

        # First ask user to focus:
        self.focus(color=color)

        # Acquire center point image to know threshold level:
        self.set_channel(color, exposure=1, intensity=5, trigger='DC')
        img = self._point_img((int(self.shape[0]/2),int(self.shape[1]/2)))
        self.set_image(img)
        self.platform.snap()
        snapped_image = self.platform.retrieve()
        threshold = np.sort(snapped_image,axis=None)[-int(snapped_image.size*1e-4)] # Keep top 0.01% of pixels

        # Create point images:
        point_images, src_points = self.point_images()

        # Load sequence:
        self.sequence(
            point_images,
            color=color,
            exposure=1,
            intensity=5,
            trigger='DC'
            )

        # Acquire:
        dst_points = []
        src_points_ref = src_points.copy()
        for src, img in zip(src_points_ref, point_images):
            # self.set_image(img)
            self.next_frame()
            self.platform.snap()
            snapped_image = self.platform.retrieve()
            dst = self._find_point(snapped_image, threshold)
            plt.imshow(snapped_image)
            if dst is None:
                print('Source point (%d, %d) not found in image'%src)
                src_points.remove(src)
            else:
                print('Source point (%d, %d) found at (%d, %d)'%(src+dst))
                dst_points.append(dst)
                plt.plot(dst[0], dst[1], 'rx')
            plt.show()
            plt.pause(.001)

        # Stop sequence:
        self.stop_sequence()

        if len(src_points) < 4:
            raise RuntimeError('Not enough points found in image, aborting calibration')

        # Compute homography:
        '''
        pts_src and pts_dst are numpy arrays of points
        in source and destination images. We need at least 4
        corresponding points.
        '''
        src_points = np.array(src_points, dtype=np.float32)
        dst_points = np.array(dst_points, dtype=np.float32)

        self.homography, _ = cv2.findHomography(
            src_points, dst_points, cv2.RANSAC, 5.0
            )

        self.homography_inverse = np.linalg.inv(self.homography)

        # Show estimation for user to double check:
        self._show_estimation()

        # Also shine a border for user to see limits:
        border_img = np.ones(self.shape,dtype=np.uint8)
        border_img[5:-5,5:-5] = 0
        estimated = self.estimate_projection(border_img)
        plt.imshow(grid(estimated))
        plt.show()



        print('Calibration done! returning to Trans...')

        self.platform.channel('Trans') # Always go back to Trans


    def calib_image(self):
        """
        Load calibration/focussing target image.

        Returns
        -------
        2D array of uint8
            Calibration target image.

        """
        # Load calibration image from disk:
        filename = os.path.join(os.path.dirname(__file__), 'calibration.tiff')
        return np.flipud((cv2.imread(filename)[:,:,0]<125).astype(np.uint8)*255)

    def point_images(self, margin=50, points_x=7, points_y=5):
        """
        Get list of arrayed points images, for calibration

        Parameters
        ----------
        margin : int, optional
            Minimum XY values. The default is 50.
        points_x : int, optional
            Number of points to show along X. The default is 7.
        points_y : int, optional
            Number of points along Y. The default is 5.

        Returns
        -------
        images : list of 2D arrays of uint8
            The images to shine onto the sample for calibration.
        fixed_points : list of tuples
            The center of the dots in each image.

        """

        grid_x = np.linspace(margin,self.shape[0]-margin,points_x).astype(int)
        grid_y = np.linspace(margin,self.shape[1]-margin,points_y).astype(int)

        fixed_points = []
        images = []

        for x in grid_x:
            for y in grid_y:
                fixed_points.append((x,y))
                images.append(self._point_img((x,y)))

        return images, fixed_points

    def _point_img(self, xy):
        """
        Create single dot image

        Parameters
        ----------
        xy : Tuple of int, int
            Coordinates of dot center.

        Returns
        -------
        2D array of uint8
            Single-dot image.

        """
        return cv2.circle(
            img = np.zeros(self.shape, dtype=np.uint8),
            center = xy,
            radius = 3,
            color=255,
            thickness=cv2.FILLED
            )

    def _find_point(self, img, threshold):
        """
        Find center of dot in acquired image

        Parameters
        ----------
        img : 2D array of uint16
            Acquired snap of single-dot image as projected by DMD.
        threshold : int
            Value to threshold image to identify dot.

        Returns
        -------
        Tuple[Int, Int]
            Coordinates of the center of the dots.

        """

        # Find point blob(s):
        contours, _ = cv2.findContours(
            (img>=threshold).astype(np.uint8),
            cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_NONE
            )

        # If no blob detected, return None:
        if len(contours)==0:
            return None

        # In unlikely event that more than 1 blob is detected, keep biggest:
        elif len(contours)>1:
            areas = [cv2.contourArea(cnt) for cnt in contours]
            contours = [contours[areas.index(max(areas))]]

        return centroid(contours[0])

    def estimate_projection(self, image):
        '''
        The calculated homography can be used to warp
        the source image to destination. Size is the
        size (width,height) of im_dst
        '''
        return cv2.warpPerspective(
            image,
            self.homography,
            # (self.mmc.get_image_height(),self.mmc.get_image_width()) #Invert for cv2
            (2048, 2048) # Temporary fix for loaner camera
            )

    def infer_image(self, image):
        """
        Infer image to load into DMD to obtain desired projection

        Parameters
        ----------
        image : 2D numpy array of uint8
            The desired projection, ie how the image projection onto the FOV
            should look when acquired with the camera.

        Returns
        -------
        image_inv : 2D numpy array of uint8
            The image to load into the DMD to obtain the desired projection.

        """

        # Invert image:
        image_inv = cv2.warpPerspective(
            image,self.homography_inverse,self.shape[::-1] #Invert for cv2
            )

        return image_inv

    def _show_estimation(self):

        # Make sure it estimates the projection correctly:
        target = self.calib_image()
        estimated = self.estimate_projection(target)
        self.set_image(target)
        self.platform.exposure(1)
        self.platform.snap()
        actual = self.platform.retrieve()
        plt.imshow(grid(estimated))
        plt.show()
        plt.imshow(grid(actual))
        plt.show()



    def sequence(
            self,
            images,
            color,
            exposure,
            intensity=None,
            time_unit='ms',
            trigger='DC',
            async_loading = False
            ):
        '''
        Set up and load sequence into DMD

        IMPORTANT: I have been getting weird behavior from the DMD with
        sequences >24 frames. It's not displaying the correct frames or even
        not displaying any frame. It may only be a problem when doing hardware
        synchronization.

        Parameters
        ----------
        images : 3D array of uint8
            Image sequence to load in the DMD. Dimensions are (frames, height, width)
            Note: If the images are not in the expected format of the DMD
            (see DMD.shape), we assume they are full-FOV _desired_ projections,
            and therefore need to be processed with infer_image. To avoid bad
            surprises, please make sure that you use infer_image ahead of time.
        color : str
            Color channel. 'red', 'green', or 'blue'.
        exposure : int
            exposure time.
        intensity : int or None, optional
            LED intensity. Valid range is 5-100. If None, left unchanged.
            The default is None.
        time_unit : str, optional
            Time unit of exposure. 'us', 'ms', or 's'. The default is 'ms'.
        trigger : str, optional
            LED trigger mode. 'DC' for computer controlled, 'Int' for internal
            timer, 'Ext' for external/hardware triggering, 'Glbl' for global lamp
            triggering mode. See X-Cite manual for more information.
            IMPORTANT: When in any other mode than DC, micromanager will completely
            freeze if you try to snap images because it will wait for an ACK that
            never comes. Other modes are only for stimulation, not imaging.
            The default is 'DC'.
        async_loading : bool, optional
            If async_loading == True, the sequence will be loaded
            asynchronously in a separate thread. Note that during this period,
            operations involving MMCore will be blocked. If you try to issue
            a command to mmc, you will receive an error like this:
            "ZMQError: Operation cannot be accomplished in current state".
            Almost all microscope operation commands are run through mmc
            (unless you are doing hardware sync via an arduino or smthg).
            To check if the sequence has finished loading, you can check if
            self._sequence_loaded == True. In the meantime you can however do
            operations with other libraries like delta, opencv etc
            The default is False

        Returns
        -------
        None or Thread
            If async_loading==True, the thread where that subroutine is
            happening will be provided (mostly for debugging purposes).

        '''

        self._sequence_loaded = False

        # If async call, self-call in thread and return thread obj:
        # JB tested this and it adds negligible overhead maybe ~10ms per call
        if async_loading:
            thr = threading.Thread(
                target=self.sequence,
                args=(images, color, exposure),
                kwargs={
                    'intensity':intensity,
                    'time_unit':time_unit,
                    'trigger':trigger,
                    'async_loading':False
                    }
                )
            thr.start()
            return thr

        # Set up lamp:
        self.set_channel(
            color,
            exposure=exposure,
            intensity=intensity,
            time_unit=time_unit,
            trigger=trigger
            )

        # Reformat images into 1D uint8 vectors in java ArrayList:
        images_1D = self.platform.bridge.construct_java_object('java.util.ArrayList')
        for img in images:
            # If image is not
            if img.shape != self.shape:
                img = self.infer_image(img)
            images_1D.add(img.ravel())

        # load images:
        self.mmc.load_slm_sequence(self.name,images_1D)

        # Start DMD sequence:
        self.mmc.start_slm_sequence(self.name)

        # For async loading:
        self._sequence_loaded = True

    def set_channel(
            self,
            color: str,
            exposure: int = None,
            time_unit: str = 'ms',
            intensity: int = None,
            trigger: str = 'DC'
            ):
        """
        Set DMD and X-Cite to channel

        Parameters
        ----------
        color : str
            The color that will be used for imaging.
        exposure : int, optional
            Exposure time. The default is None.
        time_unit : str, optional
            The unit of the exposure time. Can be 's', 'ms', 'us'.
            The default is 'ms'.
        intensity : int, optional
            X-Cite lamp itensity. Valid range is 5-100. If None, it doesn't get
            changed.
            The default is None.
        trigger : str, optional
            Triggering mode. If "DC", the DMD lamp is controlled by MM. If
            "Int" the lamp's internal clock is used to pulse light. If "Ext",
            the lamp's BNC will control the LED's state.
            The default is 'DC'.

        Returns
        -------
        None.

        """

        # Set MM channel:
        self.mmc.set_config('Channel','DMD-'+color.upper())

        # Set exposure:
        if exposure is not None:
            if trigger.lower()=='dc':
                self.mmc.set_exposure(exposure)
            elif trigger.lower()=='int':
                # set time unit:
                self.mmc.set_property(
                    self.LEDs[color.lower()],
                    'L.17 IPG Units (0:uS/1:mS/2:S)',
                    {'us':0,'ms':1,'s':2}[time_unit.lower()]
                    )
                # set exposure time:
                self.mmc.set_property(
                    self.LEDs[color.lower()],
                    'L.13 Signal On Time (0-65535)',
                    exposure
                    )
                # set off time:
                self.mmc.set_property(
                    self.LEDs[color.lower()],
                    'L.14 Signal Off Time (0-65535)',
                    1 #must be at least 1
                    )



        # Set intensity:
        if intensity is not None:
            # Set intensity:
            self.mmc.set_property(
                self.LEDs[color.lower()],
                'L.10 Intensity (0.0 or 5.0 - 100.0)%',
                intensity
                )

        # Set trigger mode:
        self.mmc.set_property(
            self.LEDs[color.lower()],
            'L.12 Pulse Mode (0:DC 1:Int 2:Ext 3:Glbl)',
            {'dc':0,'int':1,'ext':2,'glbl':3}[trigger.lower()]
            )

    def stop_sequence(self):
        """
        Stop a started DMD sequence, and reset LEDs to "DC" triggering mode.

        Returns
        -------
        None.

        """
        self.mmc.stop_slm_sequence(self.name)
        # self.set_channel('red',trigger='DC')
        # self.set_channel('green',trigger='DC')
        # self.set_channel('blue',trigger='DC')


    def next_frame(self):
        """
        Trigger next frame in the DMD sequence

        Returns
        -------
        None.

        """

        while not self._sequence_loaded:
            time.sleep(.001)

        self.platform.set(self.name,'CommandTrigger',1)


    def shine(self):
        """
        Trigger DMD lamp and wait for it to finish running

        Returns
        -------
        None.

        """

        self.platform.set('XLED1 Controller','X.09 IPG State (1=Run 0=Off)',1)

        while self.platform.get(
                'XLED1 Controller','X.09 IPG State (1=Run 0=Off)'
                )!='0':
            time.sleep(.001)

    def hw_frame(self):
        """
        Send signal to arduino to trigger next frame (but not shine), and wait
        for ACK

        Returns
        -------
        None.

        """
        if self.platform.synchronizer is None:
            raise ValueError("Synchronizer not loaded")

        self.platform.synchronizer.stimulation_sequence_start()

        while not self.platform.synchronizer.dmd_ready():
            time.sleep(.0005)

    def hw_shine_frame(self, color, exposure):
        """
        Send signal to arduino to shine lamp, trigger next frame, and wait for
        ACK

        Parameters
        ----------
        color : str
            Color for the stimulation, Can be "red", "green", or "blue".
        exposure : int
            exposure, in ms. Valid range is 0-65535

        Raises
        ------
        ValueError
            If synchronizer was not loaded for the platform.

        Returns
        -------
        None.

        """

        if self.platform.synchronizer is None:
            raise ValueError("Synchronizer not loaded")

        self.platform.synchronizer.stimulation_sequence_step(color, exposure)

        while not self.platform.synchronizer.dmd_ready():
            time.sleep(.0005)

def centroid(contour, im_height=2048):
    '''
    Get centroid of cv2 contour

    Parameters
    ----------
    contour : 3D numpy array
        Blob contour generated by cv2.findContours().
    im_height : int, optional
        Height of the original image, in pixels. The default is 2048.

    Returns
    -------
    cx : int
        X-axis coordinate of centroid.
    cy : int
        Y-axis coordinate of centroid.

    '''

    if contour.shape[0]>2: # Looks like cv2.moments treats it as an image
        # Calculate moments for each contour
        M = cv2.moments(contour)
        # Calculate x,y coordinate of center
        if M["m00"] != 0:
            cx = int(M["m10"] / M["m00"])
            cy = int(M["m01"] / M["m00"])
        else:
            cx, cy = 0, 0
    else:
        cx = int(np.mean(contour[:,:,0]))
        cy = int(np.mean(contour[:,:,1]))

    # cy = im_height-cy

    return cx,cy

def grid(img, grid_size = 250, thickness = 2):

    img = img.astype(np.float32)
    img = (img-np.min(img))/np.ptp(img)
    img = np.stack([img]*3,axis=-1)
    for y in range(0,img.shape[1],grid_size):
        for x in range(0,img.shape[0],grid_size):
            img = cv2.circle(img,(x,y),6,(0,1,0), thickness=thickness)
    # for x in range(grid_size,img.shape[0],grid_size):
    #     img = cv2.line(img,(x,0),(x,img.shape[1]),(0,1,0), thickness=thickness)
    return img
