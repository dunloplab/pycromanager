# -*- coding: utf-8 -*-
"""
This module simply implements the `NeoPixel` class with a serial connection to
the arduino that controls the NeoPixel LED ring.

"""
import time
import warnings
import serial

class NeoPixel:
    '''
    Class to control the Neopixel LED ring.
    Make sure the neopixels_python.ino sketch is loaded in the arduino before init.
    '''

    def __init__(self, port='COM9', baudrate=115200, timeout=.1):
        '''
        Instanciate

        Parameters
        ----------
        port : str, optional
            COM port of the controlling arduino. The default is 'COM9'.
        baudrate : int, optional
            Communication baud rate. Must match the value set in neopixels_python.ino.
            The default is 115200.
        timeout : float, optional
            Timeout delay, in seconds.
            The default is .1.

        Returns
        -------
        None.

        '''

        self.serial = serial.Serial(port=port, baudrate=baudrate, timeout=timeout)
        time.sleep(3) # Needs a few seconds to finish intializing
        "pyserial object to communicate with the arduino"
        self._color = [0, 0, 0]
        "Color to set the neopixel"
        self._shutter = False
        "Shutter state"
        # TODO: add handshake to make sure the arduino program is loaded
        print("%s, %d baud rate, %f s timeout"%(port, baudrate, timeout))

    def color(self, red=0, green=0, blue=0):
        '''
        Set Neopixel color

        Parameters
        ----------
        red : int, optional
            Intensity of the red LEDs, between 0 and 255.
            The default is 0.
        green : int, optional
            Intensity of the green LEDs, between 0 and 255.
            The default is 0.
        blue : int, optional
            Intensity of the blue LEDs, between 0 and 255.
            The default is 0.

        Returns
        -------
        None.

        '''

        if red < 0 or red > 255:
            warnings.warn('Red value %d is not between 0 and 255. Clipping.'%red)
            red = max(0, min(255, red))
        if green < 0 or green > 255:
            warnings.warn('Green value %d is not between 0 and 255. Clipping.'%green)
            green = max(0, min(255, green))
        if blue < 0 or blue > 255:
            warnings.warn('Blue value %d is not between 0 and 255. Clipping.'%blue)
            blue = max(0, min(255, blue))

        self._color = [int(red), int(green), int(blue)]

        if not self._shutter:
            self.serial.write(bytearray(self._color))

    def shutter(self, state=None):
        '''
        Set shutter on or off

        Parameters
        ----------
        state : None or bool, optional
            Whether the *shutter* should be ON. If the shutter is ON, the light
            is OFF. If None, the state is inverted
            The default is True.

        Returns
        -------
        None.

        '''
        if state is None:
            self._shutter = not self._shutter
        else:
            self._shutter = state

        if self._shutter:
            self.serial.write(bytearray([0,0,0]))
        else:
            self.serial.write(bytearray(self._color))

        time.sleep(.001)


    def close(self):
        '''
        Close serial connection

        Returns
        -------
        None.

        '''
        self.serial.close()
