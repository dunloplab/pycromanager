# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 19:06:01 2021

@author: jeanbaptiste
"""
import os
import threading
import time
import warnings
import gc

import cv2
import numpy as np
import delta
import tensorflow as tf
import matplotlib.pyplot as plt

class AbstractImageProcessor(threading.Thread):
    '''
    Abstract parent class for image processors.
    Each processor is dedicated to a single position.
    Inherits threading.Thread to be able to run as a daemon
    '''

    def __init__(
            self, channels, watch_channels = (0,), position_nb = 0, name = None
            ):
        '''
        Initialize object

        Parameters
        ----------
        channels : list of str
            Channel names to process.
        watch_channels : list of int, optional
            Tuple of channels to monitor and send for processing.
            The default is (0,)
        position_nb : int, optional
            Position number.
            The default is 0
        name : str or None, optional
            Name, for reference only. If None, the processor will be named
            "%s_%d"%(platform.__class__.__name__,position_nb).
            The default is None

        Returns
        -------
        None.

        '''
        super().__init__(daemon=True)

        self.position_nb = position_nb
        'Position index'
        self.name = name or "%s_%d"%(self.__class__.__name__,position_nb)
        "A name for this processor. Only used to print messages to the console"
        self.channels = list(channels)
        'Incoming channel names'
        self.watch_channels = list(watch_channels)
        'Channels indexes to monitor'
        self._sleep_time = 0.01 #10ms
        'Sleep time when waiting for incoming images'
        self.busy = False
        'Flag that process_image is running'
        self.image_queues = dict()
        'Dict of incoming images queues (1 per channel)'
        for channel in self.channels:
            self.image_queues[channel] = []
        self._counts = dict()
        'Dict of processed images counts (1 per channel)'
        for channel in self.channels:
            self._counts[channel] = 0
        self._preprocessed = False
        "Flag if preprocessing has been done"
        self._stop_flag = False
        "Flag to stop the daemon run"
        self.plots = {"preprocessing": True}

        self._serialize = [
            "position_nb",
            "name",
            "channels",
            "watch_channels",
            "_sleep_time",
            "image_queues",
            "_counts",
            "_preprocessed",
            "plots",
            ]


    def _msg(self, msg, clock=True):

        msg = "Processor %s - "%self.name +msg
        if clock:
            msg = time.ctime() +", "+ msg

        return msg

    def reset(self):
        "Reset processor (for restarts etc)"

        while self.busy:
            time.sleep(.1)

        for channel in self.channels:
            self.image_queues[channel] = []
            self._counts[channel] = 0

    def check(self):
        "Check settings"
        if not self._preprocessed:
            raise RuntimeError("Processor has not been pre-processed yet")

    def preprocess(self, firstimages):
        "Pre-processing function for processor"
        # Call actual processing hook:
        self._preprocess(firstimages)
        self._preprocessed = True

    def _preprocess(self, firstimages):
        "Actual pre-processing hook"
        # Default: do nothing
        pass

    def new_image(self, image, channel):
        'Append image to the approriate list in the image_queues dict of lists'

        if channel in self.channels:
            self.image_queues[channel].append(image)

    def pop_image(self, channel):
        'Pop out an image from the image_queues dict list and update _counts'

        if len(self.image_queues[channel])>=1:
            image = self.image_queues[channel].pop(0)
            frame_nb = self._counts[channel]
            self._counts[channel]+=1
            return image, frame_nb

        else:
            return None, None

    def process_image(self, image, channel):
        '''
        Dummy image processing function.
        This will get overriden in child classes

        Parameters
        ----------
        image : 2D numpy array
            image to process.
        channel : str
            Imaging channel of the image.

        Returns
        -------
        None.

        '''
        pass

    def run(self):
        '''
        This function is what the daemon will constantly run.
        It simply looks for images to pop out of the image_queues attribute,
        and if there is any, they are passed to process_image

        Returns
        -------
        None.

        '''

        while True and not self._stop_flag:
            self._run()
            time.sleep(self._sleep_time)

        self._stop_flag = False

    def _run(self):
        """
        The function that actually runs the queue handling etc. I took it out
        of run() this way it can be called outside of the daemon's infinite
        loop for debugging purposes.

        Returns
        -------
        None.

        """

        # Run through monitored channels:
        for channel_nb in self.watch_channels:
            channel = self.channels[channel_nb]

            # Get new image if any:
            image, frame_nb = self.pop_image(channel)
            while image is not None:
                self.busy = True
                self.process_image(image, channel_nb, frame_nb)
                image, frame_nb = self.pop_image(channel) # just to be sure

        # Wait a little bit:
        self.busy = False

    def stop(self):
        """
        Stop execution of the daemon

        Returns
        -------
        None.

        """
        self._stop = True

    def add_channel(self, channel, watch=False):

        if channel in self.channels:
            raise(f"Channel is already in list {self.channels}")

        self.channels.append(channel)
        if watch:
            self.watch_channels.append(len(self.channels)-1)
        self.image_queues[channel] = []
        self._counts[channel] = 0

    def remove_channel(self, channel):

        if channel in self.channels:
            ch_ind = self.channels.index(channel)
            self.channels.remove(channel)
            watch = self.watch_channels
            if ch_ind in watch:
                watch.remove(ch_ind)
                self.watch_channels = []
                for w in watch:
                    if w < ch_ind:
                        self.watch_channels.append(w)
                    else:
                        self.watch_channels.append(w-1)
            del self.image_queues[channel]
            del self._counts[channel]

    def copy(self):

        new = self.__class__(self.channels)
        for attr in self._serialize:
            setattr(new, attr, getattr(self, attr))
        return new

class SimpleImageSaver(AbstractImageProcessor):
    '''
    A simple class that saves images as they arrive
    '''

    def __init__(
            self,
            save_path,
            prototype = 'pos%04d/chan%02d_frame%06d.tif',
            fileorder = 'pct',
            filenamesindexing = 1,
            *args,
            **kwargs):
        '''
        Initialize object

        Parameters
        ----------
        save_path : str
            Path to save the images to.
        prototype: string, optional
            Filename prototype to save images as single-page tif images in
            a sequence folder, in C-style formatting.
            The default is 'pos%04d/chan%02d_frame%06d.tif'
        fileorder: string, optional
            Order of the numbers in the prototype, with 'p' for position number,
            'c' for imaging channels, and 't' for timepoints/frames.
            The default is 'pct'
        filenamesindexing = int
            Selects between 0-based or 1-based indexing in the filename.
            The default is 0
        *args : list
            Positional arguments for parent class (AbstractImageProcessor).
        **kwargs : dict
            Keyword arguments for parent class (AbstractImageProcessor).

        Returns
        -------
        None.

        '''
        super().__init__(*args, **kwargs)
        self.save_path = save_path
        "Path to save folder"
        self.prototype = prototype
        "Filenames string prototype"
        self.fileorder = fileorder
        "Filenames indexes order"
        self.filenamesindexing = filenamesindexing
        "0-based or 1-based ordering"

        self._serialize += ["save_path", "prototype", "fileorder", "filenamesindexing"]

    def check(self):
        super().check()
        if os.path.exists(self.save_path) and len(os.listdir(self.save_path)) > 0:
            warnings.warn("File save directory not empty: %s"%self.save_path)

    def process_image(self, image, channel_nb, frame_nb):
        '''
        Processing hook. Simply calls save_image

        Parameters
        ----------
        image : 2D numpy array
            image from input list to save to disk.
        channel_nb : int
            Channel index.
        frame_nb : int
            Frame index.

        Returns
        -------
        None.

        '''
        self.save_image(image, channel_nb, frame_nb)

    def getfilenamefromprototype(self, channel_nb: int, frame_nb: int) -> str:
        """
        Generate full filename for specific frame based on file path,
        prototype, fileorder, and filenamesindexing

        Parameters
        ----------
        channel_nb : int
            Imaging channel index (0-based indexing).
        frame_nb : int
            Frame/timepoint index (0-based indexing).

        Returns
        -------
        string
            Filename.

        """

        filenumbers = tuple(
            {"p": self.position_nb, "c": channel_nb, "t": frame_nb}[c] + self.filenamesindexing
            for c in self.fileorder
        )
        return os.path.join(self.save_path, self.prototype % filenumbers)

    def save_image(self, image, channel_nb, frame_nb):
        '''
        Save images to disk

        Parameters
        ----------
        image : 2D numpy array
            Image to save to disk.
        channel_nb : int
            Channel index.
        frame_nb : int
            Frame/timepoint index.

        Returns
        -------
        None.

        '''

        # Create filename and folder if necessary:
        filename = self.getfilenamefromprototype(channel_nb, frame_nb)
        folder = os.path.dirname(filename)
        if not os.path.exists(folder):
            os.makedirs(folder)

        # Write to disk:
        cv2.imwrite(filename, image)

    def copy(self):

        new = self.__class__(
            save_path = self.save_path,
            channels = self.channels
            )
        for attr in self._serialize:
            setattr(new, attr, getattr(self, attr))
        return new

class DummyReader():
    '''
    A class to mock the behavior of delta.utilities.xpreader
    '''
    def __init__(self):
        self.image = None

    def getframes(
        self,
        positions=None,
        channels=None,
        frames=None,
        rescale = None,
        rotate = None,
        ):

        return delta.utilities.rangescale(self.image,rescale)

class DeLTAMotherOnly(AbstractImageProcessor):
    '''
    DeLTA image analysis for the mother machine.

    This processor can be used to identify chambers and then run segmentation
    on them using DeLTA.pipeline.Position. However it does not run tracking.
    Instead, only the top cell in the image (assumed to be the mother) is
    used for feature extraction. The extracted features are in self.mothers
    '''

    def __init__(
            self,
            *args,
            delta_models = None,
            features = (
                "fluo1",
                "length",
                "width",
                "area",
                "perimeter",
                "stims"
                ),
            normalization = None,
            device = None,
            **kwargs
            ):
        '''
        Initialize object

        Parameters
        ----------
        delta_models : list of tf.keras.Model, optional
            Models as loaded by delta.utilities.load_models.
            The default is None.
        features : list of str, optional
            List of features to extract. See delta.utilities.singlecell_features.
            The default is ("length", "width", "area", "perimeter", "fluo1")
        normalization_fn : Callable, optional
            Function or method to normalize features data before storing into
            self.mothers. For example see dcc.core.data.data.normalization
        device : str or None, optional
            Tensorflow device. Can be '/device:CPU:0', '/device:GPU:0' or
            logical GPUs. If None, no specific device is assigned.
            See tf.device()
        *args : list
            Positional arguments for parent class (AbstractImageProcessor).
        **kwargs : dict
            Keyword arguments for parent class (AbstractImageProcessor).

        Returns
        -------
        None.

        '''
        super().__init__(*args, **kwargs)

        self._reader = DummyReader()
        # The position processor must be initialized at some point:
        self.delta_position = delta.pipeline.Position(
            self.position_nb, self._reader, models = delta_models
            )
        "delta.pipeline.Position object to segment images"
        self.features = features
        "List of features to extract, must be a tuple"
        self.rotation_correction = False
        "Flag to perform automated rotation correction (or provide set value)"
        self.drift = []
        "Identified drift over time"
        if normalization is not None:
            self.normalize = normalization.normalize
        else:
            self.normalize = None
        "Object to normalize mothers data"
        self.device = device
        """Tensorflow device. Can be '/device:CPU:0', '/device:GPU:0' or
        logical GPUs. If None, no specific device is assigned.
        See tf.device()"""
        self.mothers = None
        "Array of mother cells extracted features"


        self._serialize += [
            "_reader",
            "delta_position",
            "features",
            "rotation_correction",
            "drift",
            "normalize",
            "device",
            "mothers"
            ]

    def add_channel(self, channel, watch=False):

        super().add_channel(channel, watch)

        # Add to the list of features to extract:
        features = list(self.features)
        fluo_count = 0
        for feature in features:
            if feature.startswith("fluo"):
                fluo_count +=1
        features.append(f"fluo{fluo_count+1}")
        self.features = features

        # Add dimension to mothers:
        self.mothers = np.concatenate(
            (
                self.mothers,
                np.zeros(shape = self.mothers.shape[:2]+(1,),dtype=np.float32)
                ),
            axis=-1,
            )

    def _preprocess(self, firstimages, max_timepoints=1200):
        '''
        Preprocess position

        Process drift reference, chambers detection etc... and then create
        mother cells list of extracted features.

        Parameters
        ----------
        firstimage : List of 2D numpy arrays
            Reference images for pre-processing.
        max_timepoints : int, optional
            The maximum number of timepoints for the experiment,
            to initialize the mothers array. The default is 1200

        Returns
        -------
        None.

        '''


        plt.title(self.name)
        plt.imshow(firstimages[0])
        plt.show()

        # Initialize position processor:
        self.delta_position.preprocess(
            firstimages[0], rotation_correction = self.rotation_correction
            )

        # Initialize Mothers:
        self.mothers = np.zeros(
            shape = (len(self.delta_position.rois),max_timepoints,len(self.features)),
            dtype=np.float32
            )

        print("Identified %d chambers"%len(self.mothers))

        if "preprocessing" in self.plots and self.plots["preprocessing"]:
            self.plot_chambers(firstimages[0])

    def plot_chambers(self, img):

        # TODO

        img = img.astype(np.float32)
        img = 0.8*(img-np.min(img))/np.ptp(img)
        chambers_img = np.zeros_like(img)
        for roi in self.delta_position.rois:
            chambers_img = draw_chamber(chambers_img, 0.2, roi.box, (0,0), 50)

        img = np.stack((img,img,img+chambers_img),axis=2)

        plt.imshow(img)
        plt.title(self.name)
        plt.show()


    def reset(self):
        """
        Reset processor

        Returns
        -------
        None.

        """
        super().reset()
        # Re-initialize Mothers:
        self.mothers[:,:,:] = 0
        "List of mother cells extracted features"

    def process_image(self, image, channel_nb, frame_nb):
        '''
        Processing hook. Simply calls save_image

        Parameters
        ----------
        image : 2D numpy array
            image from input list to save to disk.
        channel_nb : int
            Channel index.
        frame_nb : int
            Frame index.

        Returns
        -------
        None.

        '''
        self.segment_image(image, channel_nb, frame_nb)

    def segment_image(self, image, channel_nb, frame_nb):
        '''
        Segment all ROIs in the frame:

        Parameters
        ----------
        image : 2D numpy array
            Image to process.
        channel_nb : int
            Channel index of the image.
        frame_nb : int
            Frame/timepoint index of the image.

        Returns
        -------
        None.

        '''

        # Set image as only frame in frames list:
        # self.delta_position.frames = [image]
        self._reader.image = image

        # Run segmentation on frame 0:
        with tf.device(self.device):
            self.delta_position.segment([0])

        # Store drift:
        self.drift.append((self.delta_position.drift_values))

        # Extract features:
        self.extract_features(frame_nb)

        # Get rid of single-chamber images to reduce memory footprint:
        for roi in self.delta_position.rois:
            roi.img_stack = []

        self.delta_position._msg('Segmentation done')

    def get_fluoframes(self):
        '''
        Get the latest fluo frames (ie all channels except first)

        Parameters
        ----------
        None.

        Returns
        -------
        3D numpy array or None
            Fluorescent frames, ordered as in self.channels.
            Dimensions are (channels, size_X, size_Y).

        '''

        # No fluo channels:
        if len(self.channels) == 1:
            return None

        fluo_frames  = []
        for channel in self.channels[1:]:

            # Pop out frame from queue and add to list:
            fluo_frame = self.pop_image(channel)[0]
            while fluo_frame is None:
                time.sleep(.2)
                fluo_frame = self.pop_image(channel)[0]

            fluo_frame, _ = delta.utilities.driftcorr(
                fluo_frame,
                drift = [x[-1] for x in self.delta_position.drift_values]
                )
            fluo_frames += [fluo_frame]

        # Stack and return:
        return np.stack(fluo_frames, axis=0)

    def extract_features(self, frame_nb):
        '''
        Extract mothers features

        Parameters
        ----------
        frame_nb : int
            Frame/timepoint index of the image.

        Returns
        -------
        None.

        '''

        fluo_frames = self.get_fluoframes()

        # Run through ROIs:
        for roi_nb, roi in enumerate(self.delta_position.rois):

            # Actual extraction operations:
            features = _features(roi, fluo_frames, self.features)

            # Append features to "mother" cell:
            self.append_features(roi_nb, features, frame_nb)

    def append_features(self, mother_nb, features, frame_nb):
        '''
        Append features to mother cells:

        Parameters
        ----------
        mother_nb : int
            Index of cell to update
        features : dict
            Dictionary of ectracted feature values.
        frame_nb : int
            Frame/timepoint index.

        Returns
        -------
        None.

        '''
        # Normalize if function was passed:
        if self.normalize is not None:
            features = self.normalize(features)

        # Append values to mothers:
        for k, key in enumerate(self.features):
            if key in features:
                self.mothers[mother_nb,frame_nb,k] = features[key]

class AbstractController():
    """
    Abstract class for all controllers
    """

    def __init__(self):
        """
        Initialize object

        Returns
        -------
        None.

        """

        self._dmd_images = None
        """Images to project onto sample. Initialized as None.
        Do not retrieve directly, use stimulation() instead"""
        self.dmd = None
        """Handle to the DMD object, for image inference. If None, the image will
        not be inferred. The default is None"""
        self._stims_retrieved = {'red': True, 'green': True, 'blue': True}
        "Flag if current dmd images have already been retrieved"
        self._controller = True
        "Flag that this is is a control processor" # Note: checking class bases becomes a headache when reimporting, so I do this instead

        self._serialize += ["_dmd_images", "dmd", "_stims_retrieved", "_controller"]

    def reset(self):

        self._dmd_images = None
        self._stims_retrieved = {'red': True, 'green': True, 'blue': True}

    def dmd_correction(self, dmd_images):
        """
        Infer image projection for the DMD, if self.dmd was set. See
        DMD.infer_image

        Parameters
        ----------
        dmd_images : dict
            Dict of DMD images.

        Returns
        -------
        dmd_images : dict
            Corrected DMD images.

        """

        # Infer image(s) if the DMD was set:
        if self.dmd is not None:
            for color in dmd_images:
                dmd_images[color] = self.dmd.infer_image(dmd_images[color])

        return dmd_images

    def stimulation(self, color, timeout = 60):
        """
        Retrieve the image to project onto cells, and set self.dmd_imags back
        to None. Warning: You can only access it once, then it will be erased.

        Parameters
        ----------
        color : str
            Color image to retrieve.
        timeout : float, optional
            Time in second after which, if the controller hasn't finished
            processing, the method returns None instead of the image.
            The default is 60.

        Returns
        -------
        image : 2D numpy array of uint8
            DMD image to project, under the keys 'red','green', and 'blue'.
            Return None if times out.

        """

        # If images aren't ready, wait up to timeout:
        _timeout = time.time() + timeout
        while self._stims_retrieved[color]:
            time.sleep(.005)
            if time.time() > _timeout:
                warnings.warn("stimulation image retrieval timed out")
                return None

        # Check if exists:
        if color not in self._dmd_images:
            raise ValueError(
                "DMD image color %s has not been computed by this processor"
                )

        # Get image
        image = self._dmd_images[color]

        # Update flag
        self._stims_retrieved[color] = True

        return image

class TestController(DeLTAMotherOnly, AbstractController):
    """
    A dummy controller for testing and debugging purposes. Just sends back
    an image with the position number
    """

    def __init__(self, *args, **kwargs):
        DeLTAMotherOnly.__init__(self, *args, **kwargs)
        AbstractController.__init__(self)
        self._run_delta = True
        "whether to run DeLTA on the fly (not necessary but cna be useful)"
        self.control_colors = {0 : 'red', 1 : 'green', 2 : 'blue'}
        """Dictionary indicating which color to shine depending on control
        output. For example, if control_colors = {1: 'green'}, only green
        stimulations will be applied, when the control strategy is 1. if
        control_colors = {0: 'green', 1: 'red', -1: 'blue'}, green light will
        be applied if the strategy is 0, red if it is 1, and blue if it is -1.
        The default is {0 : 'red', 1 : 'green', 2 : 'blue'}
        """

    def process_image(self, image, channel_nb, frame_nb):
        '''
        Processing hook.

        First calls DELTAMotherOnly.process_image(), then self.run_control()

        Parameters
        ----------
        image : 2D numpy array
            image from input list to save to disk.
        channel_nb : int
            Channel index.
        frame_nb : int
            Frame index.

        Returns
        -------
        None.

        '''

        # Run DELTAMotherOnly.process_image()
        if self._run_delta:
            super().process_image(image, channel_nb, frame_nb)

        # Run feedback control:
        self.run_control(image, frame_nb)

    def run_control(self, image, frame_nb):
        """
        Draw dummy images to shine on sample

        Parameters
        ----------
        image : 2D numpy array
            image from input list to save to disk.
        frame_nb : int
            Frame/timepoint index.

        Returns
        -------
        None.

        """

        # Draw images:
        params = dict(
            fontFace = cv2.FONT_HERSHEY_SIMPLEX,
            fontScale = 7,
            color = 255,
            thickness = 20,
            lineType = cv2.FILLED,
            )
        dmd_images = dict()
        for _, color in self.control_colors.items():
            I = np.zeros(shape = image.shape, dtype=np.uint8)
            cv2.putText(I,f"POS {self.position_nb}", (500,850), **params)
            cv2.putText(I,f"FRA {frame_nb}", (500,1100), **params)
            cv2.putText(I,color.upper(), (500,1350), **params)
            dmd_images[color] = np.fliplr(I)

        dmd_images = self.dmd_correction(dmd_images)

        # Update dmd images dict:
        self._dmd_images = dmd_images

        # Update retrieval flags:
        for color in dmd_images:
            self._stims_retrieved[color] = False

        print(self._msg("Stimulation images ready"))


class MotherOpenLoop(DeLTAMotherOnly, AbstractController):
    """
    Open-loop controller for the mother machine. Simply assigns a
    pre-determined stimulation
    """

    def __init__(self, *args, **kwargs):
        DeLTAMotherOnly.__init__(self, *args, **kwargs)
        AbstractController.__init__(self)
        self.control_colors = {0 : 'red', 1 : 'green', 2 : 'blue'}
        """Dictionary indicating which color to shine depending on control
        output. For example, if control_colors = {1: 'green'}, only green
        stimulations will be applied, when the control strategy is 1. if
        control_colors = {0: 'green', 1: 'red', -1: 'blue'}, green light will
        be applied if the strategy is 0, red if it is 1, and blue if it is -1.
        The default is {0 : 'red', 1 : 'green', 2 : 'blue'}
        """
        self.dmd_image_boxwidth=30
        "Width of the chamber stimulation box. The default is 30 pixels"

        self._serialize += ["control_colors", "dmd_image_boxwidth"]

    def reset(self):

        stim_ind = self.features.index("stims")
        stims = []
        for mother in self.mothers:
            stims.append(mother[:, stim_ind])
        DeLTAMotherOnly.reset(self)
        AbstractController.reset(self)
        for mother, stim in zip(self.mothers, stims):
            mother[:, stim_ind] = stim

    def assign_stimulations(self, stims):

        if stims.shape[0] !=  self.mothers.shape[0]:
            raise RuntimeError("Please assign as many stimulations as there are mothers")

        self.mothers[:,:stims.shape[1],self.features.index("stims")] = stims

    def check(self):
        """
        Check that controller is ready to run

        Returns
        -------
        None.

        """
        super().check()

        if "stims" not in self.features:
            raise RuntimeError("'stims' must be in features list")

    def process_image(self, image, channel_nb, frame_nb):
        '''
        Processing hook.

        First calls DELTAMotherOnly.process_image(), then self.run_control()

        Parameters
        ----------
        image : 2D numpy array
            image from input list to save to disk.
        channel_nb : int
            Channel index.
        frame_nb : int
            Frame index.

        Returns
        -------
        None.

        '''

        # Run DELTAMotherOnly.process_image()
        super().process_image(image, channel_nb, frame_nb)

        # Run feedback control:
        self.run_control(image, frame_nb)

    def run_control(self, image, frame_nb):
        """
        Generate DMD images from pre-determined stimulations

        Note: The stimulations for each mother must be 1D numpy array stored under
        the stims key in the self.mothers list. For example:
            self.mothers[10]['stims'] = np.random.randint(0,2,300)


        Parameters
        ----------
        image : 2D numpy array
            image from input list to save to disk.
        frame_nb : int
            Frame/timepoint index.

        Returns
        -------
        None.

        """

        # Empty images:
        dmd_images = dict()
        for _, color in self.control_colors.items():
            dmd_images[color] = np.zeros(shape = image.shape, dtype=np.uint8)

        # Stimulations index in features list:
        stim_ind = self.features.index('stims')

        # Run through strategies, update dmd images:
        for m, mother in enumerate(self.mothers):

            # Get stim for NEXT frame_nb (very important for dataset consistency)
            stim = mother[frame_nb+1,stim_ind]

            # Get stimulation color:
            color = self.control_colors[stim]

            # Draw chamber in appropriate dmd_image channel:
            dmd_images[color] = draw_chamber(
                dmd_images[color],
                value = 255,
                box = self.delta_position.rois[m].box,
                drift = [x[-1] for x in self.delta_position.drift_values],
                dmd_image_boxwidth=self.dmd_image_boxwidth
                )

        # Correct images (if self.dmd was set):
        dmd_images = self.dmd_correction(dmd_images)

        # Update dmd images dict:
        self._dmd_images = dmd_images

        # Update retrieval flags:
        for color in dmd_images:
            self._stims_retrieved[color] = False

        print(self._msg("Stimulation images ready"))

class MotherSelection(MotherOpenLoop):
    """
    Basically the same as MotherOpenLoop, except that it can identify cells
    to stimulate at the preprocessing step
    """

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.selection_feature = "fluo2"
        self.selection_level = 2000

        self._serialize += ["selection_feature", "selection_level"]

    def _preprocess(self, firstimages, *args, **kwargs):

        '''
        Preprocess position

        Runs parent class DELTAMotherOnly.preprocess() first then adds 'stims'
        key to mothers attribute.

        Parameters
        ----------
        *args : list
            Positional arguments for parent class (DELTAMotherOnly).
        **kwargs : dict
            Keyword arguments for parent class (DELTAMotherOnly).

        Returns
        -------
        None.

        '''
        # Run DELTAMotherOnly._preprocess():
        super()._preprocess(firstimages, *args, **kwargs)

        # Run segmentation:
        self._reader.image = firstimages[0]
        self.delta_position.segment([0])

        # List of features to extract:
        features_list = self.features
        if self.selection_feature not in features_list:
            features_list = features_list + (self.selection_feature,)

        if len(firstimages) >= 2:
            fluo_frames = np.array(firstimages[1:])
        else:
            fluo_frames = None


        # Process image:
        stim_ind = self.features.index('stims')
        for roi, mother in zip(self.delta_position.rois,self.mothers):
            features = _features(roi, fluo_frames, features_list)
            if self.selection_feature in features and features[self.selection_feature] >= self.selection_level:
                mother[:,stim_ind] = 1
            else:
                mother[:,stim_ind] = 0


        if "preprocessing" in self.plots and self.plots["preprocessing"]:
            self.plot_selection(firstimages[-1])


    def plot_selection(self, img):
        """
        This simply plots an image showing what chambers were chosen for
        selection.
        """

        img = img.astype(np.float32)
        img = 0.8*(img-np.min(img))/np.ptp(img)
        chambers_img = np.zeros_like(img)
        for roi, mother in zip(self.delta_position.rois,self.mothers):
            if mother[-1,self.features.index("stims")]==1:
                chambers_img = draw_chamber(chambers_img, 0.2, roi.box, (0,0), 50)

        img = np.stack((img,img,img+chambers_img),axis=2)

        plt.imshow(img)
        plt.title(self.name)
        plt.show()

class MotherFeedback(DeLTAMotherOnly, AbstractController):
    """
    Feedback controller for the mother machine. Only controls top / mother cell
    """

    def __init__(self,  *args, control_server = None, control_features_ind = None, **kwargs):
        """
        Initialize object

        Parameters
        ----------
        control_server : dcc.core.control.server.Server, optional
            Feedback control "server".
            The default is None.
        control_features_ind : List of int, optional
            The list of feature indexes that need to be passed to the controller.
            The indexes refer to the self.feature list. If None, the full
            list of features is used.
            The default is None.
        *args : list
            Positional arguments for parent class (DeLTAMotherOnly).
        **kwargs : dict
            Keyword arguments for parent class (DeLTAMotherOnly).

        Returns
        -------
        None.

        """
        # Init parent classes:
        DeLTAMotherOnly.__init__(self, *args, **kwargs)
        AbstractController.__init__(self)
        self.control_server = control_server
        "Controller server object. See deepcellcontrol.server"
        self.control_colors = {0 : 'red', 1 : 'green'}
        """Dictionary indicating which color to shine depending on control
        output. For example, if control_colors = {1: 'green'}, only green
        stimulations will be applied, when the control strategy is 1. if
        control_colors = {0: 'green', 1: 'red', -1: 'blue'}, green light will
        be applied if the strategy is 0, red if it is 1, and blue if it is -1.
        The default is {0 : 'red', 1 : 'green'}
        """
        self.dmd_image_boxwidth=30
        "Width of the chamber stimulation box. The default is 30 pixels"
        self.objectives = None
        "Control objectives. Dimensions are (cells, time)"
        self.normalized_objectives = None
        "Control objectives, normalized to the same range as fluo1"
        self.past_steps = 36
        "Minimum number of past timepoints to feed into controller"
        self.horizon = 24
        "Control horizon"
        self.control_features_ind = control_features_ind or list(range(len(self.features)))
        "Features indexes to pass to the controller."

    def reset(self):

        # Make sure both parent classes reset() methods are run, otherwise
        # by default super only runs the first parent class that has the
        # corresponding method:
        objectives = []
        for mother in self.mothers:
            objectives.append(mother["objective"])
        DeLTAMotherOnly.reset(self)
        AbstractController.reset(self)
        for mother, objective in zip(self.mothers, objectives):
            mother['stims'] = []
            mother['objective'] = objective

    def _preprocess(self, *args, **kwargs):
        '''
        Preprocess position

        Runs parent class DELTAMotherOnly.preprocess() first then adds 'stims'
        key to mothers attribute.

        Parameters
        ----------
        *args : list
            Positional arguments for parent class (DELTAMotherOnly).
        **kwargs : dict
            Keyword arguments for parent class (DELTAMotherOnly).

        Returns
        -------
        None.

        '''
        # Run DELTAMotherOnly._preprocess():
        super()._preprocess(*args, **kwargs)

    def assign_objectives(self, objectives):
        """
        Assign objectives to each cell

        Parameters
        ----------
        objectives : 2D numpy array of float32
            Objectives for each cell in position. DO NOT normalize, the
            objectives will be normalized in this function to the fluo1 range.
            Dimensions are (cells, time)

        Raises
        ------
        RuntimeError
            If the number of objectives given does not match the number of
            detected chambers / mothers in the position.

        Returns
        -------
        None.

        """

        if len(objectives) != len(self.mothers):
            raise RuntimeError("Please assign as many objectives as there are mothers")

        self.objectives = objectives

        self.normalized_objectives = self.normalize({"fluo1": objectives})["fluo1"]


    def check(self):
        """
        Check that controler is ready to run

        Returns
        -------
        None.

        """
        super().check()

        if self.objectives is None:
            raise RuntimeError("You did not assign objectives")


    def process_image(self, image, channel_nb, frame_nb):
        '''
        Processing hook.

        First calls DELTAMotherOnly.process_image(), then self.run_control()

        Parameters
        ----------
        image : 2D numpy array
            image from input list to save to disk.
        channel_nb : int
            Channel index.
        frame_nb : int
            Frame index.

        Returns
        -------
        None.

        '''

        # Run DELTAMotherOnly.process_image()
        super().process_image(image, channel_nb, frame_nb)

        # Run feedback control:
        self.run_control(image, frame_nb)

    def run_control(self, image, frame_nb):
        """
        Send control inputs to control server

        Parameters
        ----------
        image : 2D numpy array
            image from input list to save to disk.
        frame_nb : int
            Frame/timepoint index.

        Returns
        -------
        None.

        """

        # If not enough time points yet, default control outputs:
        if frame_nb < self.past_steps-1:
            self.dispatch_handler(
                [False]*len(self.mothers),
                {'pos':self.position_nb,'frame_nb':frame_nb,'imshape':image.shape}
                )
            return

        # Send feedback inputs, metadata, and dispatch method to control server:
        inputs = (
            self.mothers[:,:frame_nb+1,self.control_features_ind], # timepoint frame_nb must be included
            self.normalized_objectives[:,frame_nb:frame_nb+self.horizon]
            )
        self.control_server.queue.put(
            (
                inputs,
                {'pos':self.position_nb,'frame_nb':frame_nb,'imshape':image.shape},
                self.dispatch_handler
                )
            )

    def dispatch_handler(self, strategies, metadata):
        """
        Method to handle dispatches from the controller server

        Parameters
        ----------
        strategies : 1D numpy array
            Control strategies for each cell.
        metadata :  dict
            Processing metadata, contains frame number and image size.

        Returns
        -------
        None.

        """

        # Empty images:
        dmd_images = dict()
        for _, color in self.control_colors.items():
            dmd_images[color] = np.zeros(shape = metadata['imshape'], dtype=np.uint8)

        # A bit hacky:
        for chamber_nb, strategy in enumerate(strategies):
            if self.normalized_objectives[chamber_nb,metadata['frame_nb']] < .01:
                strategies[chamber_nb] = 0
            elif self.normalized_objectives[chamber_nb,metadata['frame_nb']] > .99:
                strategies[chamber_nb] = 1

        # Store strategies in 'stims' dim of self.mothers:
        self.mothers[:,metadata['frame_nb']+1,self.features.index("stims")] = strategies

        # If neighbor stims are also collected, also add these:
        if "neighbor_stims" in self.features:
            strat_left = np.roll(strategies,1)
            strat_left[0] = 0
            strat_right = np.roll(strategies,-1)
            strat_right[-1] = 0
            self.mothers[
                :,metadata['frame_nb']+1,self.features.index("neighbor_stims")
                ] = strat_left + strat_right


        # Run through strategies, update dmd images:
        for chamber_nb, strategy in enumerate(strategies):

            # Get stimulation color:
            color = self.control_colors[strategy]

            # Draw chamber in appropriate dmd_image channel:
            dmd_images[color] = draw_chamber(
                dmd_images[color],
                value = 255,
                box = self.delta_position.rois[chamber_nb].box,
                drift = [x[-1] for x in self.delta_position.drift_values],
                dmd_image_boxwidth=self.dmd_image_boxwidth
                )

        # Correct images (if self.dmd was set):
        dmd_images = self.dmd_correction(dmd_images)

        # Update dmd images dict:
        self._dmd_images = dmd_images

        # Update retrieval flags:
        for color in dmd_images:
            self._stims_retrieved[color] = False

        del strategies
        del metadata
        gc.collect()

        print(self._msg("Stimulation images ready"))

# def MotherSelection(MotherOpenLoop):


def _features(roi, fluo_frames, features_list, min_area = 200):
    """
    Actual extraction operations

    Parameters
    ----------
    roi : delta.pipeline.roi object
        The ROI object for a single chamber.
    fluo_frames : 3D numpy array or None
        Fluorescence images.
    features_list : List of str
        List of features to extract
    min_area : int
        Minimum cell area. If a segmented cell is smaller, the cell is filtered
        out from the seg mask before extracting features.

    Returns
    -------
    features : dict
        Dictionary of extracted features.

    """

    # Init dict:
    features = dict()

    # Image sharpness:
    if "sharpness" in features_list:
        features["sharpness"] = np.mean(
            cv2.Laplacian((roi.img_stack[0]*255).astype(np.uint8), 2)
            )

    # Resize seg output:
    resize = (
        roi.box["xbr"] - roi.box["xtl"],
        roi.box["ybr"] - roi.box["ytl"],
    )
    seg = cv2.resize(
        roi.seg_stack[0], resize, interpolation=cv2.INTER_NEAREST
    )

    # Remove cells that are too small:
    seg = delta.utilities.opencv_areafilt(seg, min_area = min_area)

    # Get cell contours:
    contours = delta.utilities.find_contours(seg)
    if "cell_count" in features_list:
        features["cell_count"] = len(contours)

    # If no cells, return:
    if len(contours) == 0:
        return features

    # Get mother contour:
    contours.sort(key=lambda elem: np.max(elem[:, 0, 1]))  # Sorting along Y
    mother_cnt = contours[0]

    # Get mother mask:
    mother_mask = np.zeros_like(seg)
    mother_mask = cv2.drawContours(
        mother_mask, [mother_cnt], 0, color=1, thickness=-1
        )

    # Extract delta features and update features dict:
    features.update(
        delta.utilities.singlecell_features(
            contour=mother_cnt,
            mask=mother_mask,
            features=features_list,
            fluo_frames = fluo_frames,
            roi_box = roi.box,
            )
        )

    # Other cells fluo:
    other_fluos = []
    for f in features_list:
        for ref in ("chamber_mean_","chamber_median_","chamber_std_"):
            if f.startswith(ref):
                fluo_name = f[len(ref):]
                if not fluo_name in other_fluos:
                    other_fluos.append(fluo_name)

    for fluo_name in other_fluos:
        all_fluo = [features[fluo_name]]
        if len(contours) > 1:
            for cell in contours[1:]:
                mother_mask.fill(0.)
                mother_mask = cv2.drawContours(
                    mother_mask, [cell], 0, color=1, thickness=-1
                    )
                all_fluo.append(
                    delta.utilities.singlecell_features(
                        contour=cell,
                        mask=mother_mask,
                        features=[fluo_name],
                        fluo_frames = fluo_frames,
                        roi_box = roi.box,
                        )[fluo_name]
                    )

        if "chamber_mean_" + fluo_name in features_list:
            features["chamber_mean_" + fluo_name] = np.mean(all_fluo)

        if "chamber_median_" + fluo_name in features_list:
            features["chamber_median_" + fluo_name] = np.median(all_fluo)

        if "chamber_std_" + fluo_name in features_list:
            features["chamber_std_" + fluo_name] = np.std(all_fluo)

    return features




def draw_chamber(img, value, box, drift, dmd_image_boxwidth):
    """
    Draw stimulation for single mother machine chamber

    Parameters
    ----------
    img : 2D numpy array
        Image to draw on.
    value : int
        Value/intensity to draw for the chamber.
    box : dict
        Dictionary of box position and dimensions
        (see delta.utilities.CroppingBox).
    drift : tuple of (int,int)
        XY drift to add to box position.
    dmd_image_boxwidth : int
        Width of the chamber illumination box to draw.

    Returns
    -------
    img : 2D numpy array
        Image with drawn chamber.

    """

    # Process box:
    ytl = box['ytl']+round(drift[0])-30 # Adding constant shift here to be safe
    if ytl < 0: ytl = 0
    if ytl > img.shape[0]-1: return img# If top left corner has passed image height: do nothing
    ybr = box['ybr']+round(drift[0])
    if ybr > img.shape[0]-1: ybr = img.shape[0]-1
    if ybr < 0: return img
    xtl = box['xtl']+round(drift[1])+round((box['xbr']-box['xtl']-dmd_image_boxwidth)/2)
    if xtl < 0: xtl = 0
    if xtl > img.shape[1]-1: return img
    xbr = box['xbr']+round(drift[1])-round((box['xbr']-box['xtl']-dmd_image_boxwidth)/2)
    if xbr > img.shape[1]-1: xbr = img.shape[1]-1
    if xbr < 0: return img

    # Draw box:
    img[int(ytl):int(ybr),int(xtl):int(xbr)] = value

    return img
