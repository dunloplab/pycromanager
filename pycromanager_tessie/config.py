#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  2 17:02:44 2021

@author: jeanbaptiste
"""

VALID_TRANS_CHANNELS = ['Trans']

VALID_FLUO_CHANNELS = [
    'CFP',
     'DAPI',
     'GFP',
     'RFP',
     'YFP'
     ]