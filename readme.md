# pycromanager_tessie

`pycromanager_tessie` is a (mostly) python package to conduct high-throughput microscopy experiments through the
micromanager and pycromanager APIs. It also interfaces with our `DeLTA` and `deepcellcontrol` packages to conduct
on the fly image analysis and feedback control. The following manuscript uses this package:

[Lugagne J-B, Blassick CM, & Dunlop MJ (2022) Deep model predictive control of gene expression in thousands of single cells. _bioRxiv_](https://www.biorxiv.org/content/10.1101/2022.10.28.514305v1)

Feel free to use this code as inspiration, but a lot of hardware aspects are implemented specifically for our microscope :heart: Tessie :heart:

:scroll: To get started with this package, check out the documentation 
[here](https://dunloplab.gitlab.io/pycromanager/public/)
