# -*- coding: utf-8 -*-
"""
Created on Wed Apr  6 14:38:38 2022

@author: jeanbaptiste
"""

import pycromanager_tessie as pmt
from pycromanager_tessie.acquisitions import experiment

# Start platform:
platform = pmt.microscope.platform.Platform(load_synchronizer=True)

# Set LED ring
platform.neopixel.color(red=int(255/4))

# Reproducibility settings:
platform.set("Andor sCMOS Camera","Sensitivity/DynamicRange","12-bit (low noise)")
platform.set("Spectra","White_Level",5)
platform.set("DiaLamp","Intensity",200)

#%% Calibrate DMD:
platform.neopixel.shutter(True)
platform.dmd.calibrate(color = 'blue')
platform.neopixel.shutter(False)

#%% Experimental parameters:


# Init experiment object:
xp = experiment.MoMaOpenLoopExperiment(
    platform,
    save_folder = "E:/JBL/deepmpc/trainingsets/2022-04-23_TrainingSet7",
    delta_config = "C:/Users/System 6/Documents/config_mothermachine.json"
    )
xp.stim_batches = None
xp.period = 300
xp.features = (
    "length",
    "width",
    "area",
    "perimeter",
    "cell_count",
    "fluo1",
    "sharpness",
    "chamber_mean_fluo1",
    "chamber_median_fluo1",
    "chamber_std_fluo1"
    )

# Channels:
trans_settings = pmt.acquisitions.series.ChannelSettings(
    name='Trans', exposure=85, trigger = "Ext"
    )
gfp_settings = pmt.acquisitions.series.ChannelSettings(
    name='GFP', exposure=85, trigger = "Ext"
    )

# Stimulations:
reddmd_settings = pmt.acquisitions.series.StimulationsSettings(
    color='red', exposure=60, intensity = 100, trigger = "Ext"
    )

# Acquisition series settings: (all using the same settings here)
series_settings = pmt.acquisitions.series.SeriesSettings(
    acquisition_mode = 'simultaneous',
    channels = [trans_settings, gfp_settings],
    stimulations = [reddmd_settings]
    )

#%% Append new series (run this cell as many times as you want series)

# Create single acquisition series:
series = pmt.acquisitions.series.Series(
    series_number=len(xp.series), platform=platform, settings=series_settings
    )

# Get line of positions:
series.positions_line(index_offset=xp.total_positions())

xp.series.append(series)


#%% Pre-process positions (ie acquire reference images & init processors):  

# Preprocess:
xp.preprocess()

# Get random stimulations:
stims = experiment.dcc.utilities.random_stimulations(
    total_simulations=xp.total_chambers()
    )

# Assign 2*200 "reference" cells to go through pre-set cycles:
import numpy as np
total_refs = 400 # divisible by 2
references = np.random.choice(xp.total_chambers(), total_refs, replace = False)
stims[references[:int(total_refs/2)],:] = 36*[False] + (8*12*[True] + 8*12*[False])*2 + 12*[True]
stims[references[int(total_refs/2):],:] = 36*[False] + (8*12*[False] + 8*12*[True])*2 + 12*[False]

# Assign to cells:
xp.assign_stimulations(stims.astype(int))

#%% Run experiment:
platform.neopixel.color(green=1)

xp.run()

#%% Save & clean up:

xp.postprocess()

#%% Show reference curves + histogram of non-reference cells (optional):

# Go through mother cells and sort between ref and non-ref cells:
cell_nb = 0
fluo_ref1 = []
fluo_ref2 = []
fluo_nonref = []
stims_nonref = []
cell_nb_nonref = []
for series in xp.series:
    for position in series.positions:
        controller = position.processors["controller"]
        for mother in controller.mothers:
            if cell_nb in references:
                if np.where(references == cell_nb)[0][0] < 200:
                    fluo_ref1.append(mother["fluo1"])
                else:
                    fluo_ref2.append(mother["fluo1"])
            else:
                fluo_nonref.append(mother["fluo1"])
                stims_nonref.append(mother["stims"])
                cell_nb_nonref += [cell_nb]
                
            cell_nb += 1

import os
os.makedirs(xp.save_folder+"/reference_plots/", exist_ok=True)

import matplotlib.pyplot as plt
from pmt.dcc.utilities import plotq
plotq(fluo_ref1)
plotq(fluo_ref2,color="orange")
plt.xlabel("time (hours)")
plt.ylabel("GFP (a.u.)")
plt.ylim([0, 4_095])
plt.grid(which="both", axis="both")
plt.title("Reference curves")
plt.savefig(xp.save_folder+"/reference_plots/reference_cells.png", dpi=300)
plt.savefig(xp.save_folder+"/reference_plots/reference_cells.svg", dpi=300)
plt.show()


# Plot histogram of all cells after 6 hours:
plt.hist(np.array(fluo_nonref).flatten(), bins=100)
plt.xlabel("GFP (a.u.)")
plt.ylabel("count (cells x timepoints)")
plt.grid(which="both", axis="both")
plt.title("Non-ref cells, histogram 6h to end time")
plt.savefig(xp.save_folder+"/reference_plots/total_histogram.png", dpi=300)
plt.savefig(xp.save_folder+"/reference_plots/total_histogram.svg", dpi=300)
plt.show()

# Plot fluo response of 100 random cells:
x = np.arange(0,len(xp._acq_timepoints))/12
for i in np.random.choice(len(cell_nb_nonref), 100, replace = False):
    c = cell_nb_nonref[i]
    stim_seq = stims_nonref[i][:len(xp._acq_timepoints)]
    experiment.dcc.utilities.OptoPlotBackground(stim_seq,x, ymax = 4095)
    plt.plot(x, fluo_nonref[i])
    plt.xlim([0, len(xp._acq_timepoints)/12])
    plt.ylim([0, 4_095])
    plt.xlabel("time (hours)")
    plt.ylabel("GFP (a.u.)")
    plt.title(f"Cell #{c}")
    plt.savefig(xp.save_folder+f"/reference_plots/cell_{c:06d}.png", dpi=300)
    plt.savefig(xp.save_folder+f"/reference_plots/cell_{c:06d}.svg", dpi=300)
    plt.show()

#%% Run full delta pipeline:

xp.delta_pipeline()
