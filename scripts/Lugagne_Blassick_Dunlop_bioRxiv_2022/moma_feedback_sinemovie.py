# -*- coding: utf-8 -*-
"""
Created on Sun May  1 17:33:44 2022

@author: jeanbaptiste
"""

import os
import json

import numpy as np

import pycromanager_tessie as pmt
from pycromanager_tessie.acquisitions import experiment

# Start platform:
platform = pmt.microscope.platform.Platform(load_synchronizer=True)

# Set LED ring
platform.neopixel.color(red=int(255/4))

# Reproducibility settings:
platform.set("Andor sCMOS Camera","Sensitivity/DynamicRange","12-bit (low noise)")
platform.set("Spectra","White_Level",5)
platform.set("DiaLamp","Intensity",200)

#%% Calibrate DMD:
platform.neopixel.shutter(True)
platform.dmd.calibrate(color = 'blue')
platform.neopixel.shutter(False)

#%% Experimental parameters:

# FALLBACK Control parameters:
control_model_folder = control_model_folder = "C:/Users/System 6/Documents/deepcellcontrol/assets/models/2022-05-07_21-52-16_f651d065-2d7b-4f55-8dee-10e48d6e79b2"
with open(control_model_folder + "/training_parameters.json", "r") as f:
    control_parameters = json.load(f)
control_parameters["model_file"] = control_model_folder + "/model.hdf5"
control_parameters["bpso_iterations"] = 10
control_parameters["bpso_particles"] = 20

# Init experiment object:
xp = experiment.MoMaFeedbackExperiment(
    platform,
    save_folder = "E:/JBL/deepmpc/control/2022-05-11_DeepMPC_sinemovie_3",
    delta_config = "C:/Users/System 6/Documents/config_mothermachine.json",
    control_parameters = control_parameters,
    )
xp.stim_batches = 13

# Channels:
trans_settings = pmt.acquisitions.series.ChannelSettings(
    name='Trans', exposure=85, trigger = "Ext"
    )
gfp_settings = pmt.acquisitions.series.ChannelSettings(
    name='GFP', exposure=85, trigger = "Ext"
    )

# Stimulations:
reddmd_settings = pmt.acquisitions.series.StimulationsSettings(
    color='red', exposure=60, intensity = 100, trigger = "Ext"
    )

# Acquisition series settings: (all using the same settings here)
series_settings = pmt.acquisitions.series.SeriesSettings(
    acquisition_mode = 'simultaneous',
    channels = [trans_settings, gfp_settings],
    stimulations = [reddmd_settings]
    )


#%% Append new series (run this cell as many times as you want series)

# Create single acquisition series:
series = pmt.acquisitions.series.Series(
    series_number=len(xp.series), platform=platform, settings=series_settings
    )

# Get line of positions:
series.positions_line(index_offset=xp.total_positions())

xp.series.append(series)


#%% Pre-process positions (ie acquire reference images & init processors):  

# Preprocess:
xp.preprocess()

#%% Generate objectives

# Sine-waves movie control objective:
objectives = experiment.dcc.utilities.concentric_sines_objectives(
    100, offset=1250, amplitude= 750, prop_speed=3, period=8*60, duration=48*60
    )
objectives = objectives[:,int(24*60/5):] # Use only the last 24 hours to make sure all cells are controlled to something
objectives = np.pad(objectives, ((0,0),(36,0)))

# Randomize objectives:
rng = np.random.default_rng(1)
shuffling = rng.choice(
    np.arange(objectives.shape[0]), size=objectives.shape[0], replace=False
    )

deshuffling = shuffling.copy()
for i, s in enumerate(shuffling):
    deshuffling[s] = i

objectives = objectives[shuffling, :]
objectives = objectives.astype(np.float32)

np.save(xp.save_folder+"whole_movie_objectives.npy", objectives)
np.save(xp.save_folder+"whole_movie_shuffling.npy", shuffling)
np.save(xp.save_folder+"whole_movie_deshuffling.npy", deshuffling)

#%% Re-load and assign objectives:

# Previous (or current) folder
objectives_folder = "E:/JBL/deepmpc/control/2022-05-11_DeepMPC_sinemovie_2/"

objectives = np.load(objectives_folder+"whole_movie_objectives.npy")
shuffling = np.load(objectives_folder+"whole_movie_shuffling.npy")
deshuffling = np.load(objectives_folder+"whole_movie_deshuffling.npy")

np.save(xp.save_folder+"whole_movie_objectives.npy", objectives)
np.save(xp.save_folder+"whole_movie_shuffling.npy", shuffling)
np.save(xp.save_folder+"whole_movie_deshuffling.npy", deshuffling)

# Previous cells performance:
if os.path.exists(objectives_folder+"cells_rmse.npy"):
    cells_rmse = np.load(objectives_folder+"cells_rmse.npy")
else:
    cells_rmse = []

# Cells already processed in previous experiments:
already_processed = len(cells_rmse)
print("!"*80+"\nGoing to run objectives starting from cell %d\n"%already_processed+"!"*80)

# What objectives should be run:
if already_processed + xp.total_chambers() < objectives.shape[0]:
    local_obj = objectives[already_processed:already_processed+xp.total_chambers()]
    local_shuffle = shuffling[already_processed:already_processed+xp.total_chambers()]
    local_deshuffle = deshuffling[already_processed:already_processed+xp.total_chambers()]
else:
    cells_ranked = np.argsort(cells_rmse)
    objectives_ranked = objectives[cells_ranked]
    shuffle_ranked = shuffling[cells_ranked]
    deshuffle_ranked = deshuffling[cells_ranked]
    local_obj = np.concatenate(
        (
            objectives[already_processed:],
            objectives_ranked[
                objectives.shape[0]-already_processed-xp.total_chambers():
                ]
            ),
        axis=0
        )
    local_shuffle = np.concatenate(
        (
            shuffling[already_processed:],
            shuffle_ranked[
                objectives.shape[0]-already_processed-xp.total_chambers():
                ]
            ),
        axis=0
        )
    local_deshuffle = np.concatenate(
        (
            deshuffling[already_processed:],
            deshuffle_ranked[
                objectives.shape[0]-already_processed-xp.total_chambers():
                ]
            ),
        axis=0
        )
    np.save(xp.save_folder+"previouscells_ranked.npy", local_obj)

# Saving to disk:
np.save(xp.save_folder+"local_objectives.npy", local_obj)
np.save(xp.save_folder+"local_shuffling.npy", local_shuffle)
np.save(xp.save_folder+"local_deshuffling.npy", local_deshuffle)

# Assign to cells:
xp.assign_objectives(local_obj)

import sys
xp.logger.log.close()
sys.stdout = xp.logger.terminal

#%% Run experiment:
platform.neopixel.color(green=1)

xp.run()

#%% Save & clean up:

xp.postprocess()

#%% Quickly plots:


# Cells data:
import pickle
with open(xp.save_folder+"/mothers.pkl", "rb") as f:
    mothers = pickle.load(f)
    
        
# Compute RMSE per cell (after 3+3 hours):
comp_start = 6*12
cutoff = 19*12
local_cells_rmse = []
cells_fluo = []
cells_stims = []
x = np.arange(cutoff)/12
cell_nb = 0
for s in mothers:
    for p in s:
        for mother in p:
            fluo = mother[:cutoff, 0]*4095
            obj = local_obj[cell_nb, :cutoff]
            stims = mother[:cutoff, -1]*4095
            rmse = np.sqrt(np.mean((fluo[comp_start:]-obj[comp_start:])**2))
            cell_nb += 1
            
            cells_fluo.append(fluo)
            cells_stims.append(stims)
            local_cells_rmse.append(rmse)

cells_fluo = np.array(cells_fluo)
cells_stims = np.array(cells_stims)
local_cells_rmse = np.array(local_cells_rmse)
np.save(xp.save_folder+"/local_cells_rmse.npy", local_cells_rmse)
np.save(
    xp.save_folder+"/cells_rmse.npy",
    np.concatenate((cells_rmse, local_cells_rmse), axis=0)
    )
np.save(xp.save_folder+"/cells_fluo.npy", cells_fluo)
np.save(xp.save_folder+"/cells_stims.npy", cells_stims)


import os
os.makedirs(xp.save_folder+"/reference_plots/", exist_ok=True)

import matplotlib.pyplot as plt
for i in np.random.choice(len(cells_fluo), 100, replace = False):
    stim_seq = cells_stims[i]
    experiment.dcc.utilities.OptoPlotBackground(stim_seq,x, ymax = 1)
    plt.plot(x, local_obj[i,:cutoff]/4095,"--k")
    plt.plot(x, cells_fluo[i]/4095,"b")
    plt.xlim([0, cutoff/12])
    plt.ylim([0, 1])
    plt.xlabel("time (hours)")
    plt.ylabel("GFP (a.u.)")
    plt.title(f"Cell #{i}, RMSE = {local_cells_rmse[i]:.2f}")
    plt.savefig(xp.save_folder+f"/reference_plots/cell_{i:06d}.png", dpi=300)
    plt.savefig(xp.save_folder+f"/reference_plots/cell_{i:06d}.svg", dpi=300)
    plt.show()
