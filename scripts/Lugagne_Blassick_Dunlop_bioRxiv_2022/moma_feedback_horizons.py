# -*- coding: utf-8 -*-
"""
Created on Sun May  1 17:33:44 2022

@author: jeanbaptiste
"""

import json
import pycromanager_tessie as pmt
from pycromanager_tessie.acquisitions import experiment

# Start platform:
platform = pmt.microscope.platform.Platform(load_synchronizer=True)

# Set LED ring
platform.neopixel.color(red=int(255/4))

# Reproducibility settings:
platform.set("Andor sCMOS Camera","Sensitivity/DynamicRange","12-bit (low noise)")
platform.set("Spectra","White_Level",5)
platform.set("DiaLamp","Intensity",200)

#%% Calibrate DMD:
platform.neopixel.shutter(True)
platform.dmd.calibrate(color = 'blue')
platform.neopixel.shutter(False)

#%% Experimental parameters:
model_folders = {
    "horizon_12": "C:/Users/System 6/Documents/deepcellcontrol/assets/models/2022-05-07_21-35-35_f124b5d8-48ea-42a8-aba1-5b5a89b4f264",
    "horizon_24": "C:/Users/System 6/Documents/deepcellcontrol/assets/models/2022-05-07_21-52-16_f651d065-2d7b-4f55-8dee-10e48d6e79b2",
    "horizon_36": "C:/Users/System 6/Documents/deepcellcontrol/assets/models/2022-05-07_22-09-22_463ed82e-1a2e-484a-9b6e-b4c570e07db7",
    "horizon_48": "C:/Users/System 6/Documents/deepcellcontrol/assets/models/2022-05-07_22-27-15_226cd785-dc7e-4fa2-b302-0187d904f22c",
    }

# Control parameters:
control_model_folder = model_folders["horizon_24"]
with open(control_model_folder + "/training_parameters.json", "r") as f:
    control_parameters = json.load(f)
control_parameters["model_file"] = control_model_folder + "/model.hdf5"
control_parameters["bpso_iterations"] = 25
control_parameters["bpso_particles"] = 40

# Init experiment object:
xp = experiment.MoMaFeedbackExperiment(
    platform,
    save_folder = "E:/JBL/deepmpc/control/2022-05-07_DeepMPC_horizons_tests",
    delta_config = "C:/Users/System 6/Documents/config_mothermachine.json",
    control_parameters = control_parameters,
    )
xp.stim_batches = 9

# Channels:
trans_settings = pmt.acquisitions.series.ChannelSettings(
    name='Trans', exposure=85, trigger = "Ext"
    )
gfp_settings = pmt.acquisitions.series.ChannelSettings(
    name='GFP', exposure=85, trigger = "Ext"
    )

# Stimulations:
reddmd_settings = pmt.acquisitions.series.StimulationsSettings(
    color='red', exposure=60, intensity = 100, trigger = "Ext"
    )

# Acquisition series settings: (all using the same settings here)
series_settings = pmt.acquisitions.series.SeriesSettings(
    acquisition_mode = 'simultaneous',
    channels = [trans_settings, gfp_settings],
    stimulations = [reddmd_settings]
    )


#%% Append new series (run this cell as many times as you want series)

# Create single acquisition series:
series = pmt.acquisitions.series.Series(
    series_number=len(xp.series), platform=platform, settings=series_settings
    )

# Get line of positions:
series.positions_line(index_offset=xp.total_positions())

xp.series.append(series)


#%% Pre-process positions (ie acquire reference images & init processors):  

# Preprocess:
xp.preprocess()

# Get sine objective:
objectives = experiment.dcc.utilities.sine_objective(
                    period=8*60,
                    offset=1250,
                    amplitude=750,
                    delay=0,
                    duration=48*60,
                    sampling=5
                    )
import numpy as np
objectives = np.pad(objectives, (36,0))
objectives = objectives[np.newaxis,:].astype(np.float32)
objectives = np.repeat(objectives, repeats = xp.total_chambers(), axis = 0)

# Assign to cells:
xp.assign_objectives(objectives)

#%% Update control servers:

# Divide positions into different partitions for each server:
positions_part = np.random.choice(
    xp.total_positions(),
    size = xp.total_positions(),
    replace = False
    )
positions_part = [
    positions_part[:19],
    positions_part[19:38],
    positions_part[38:57],
    positions_part[57:]
    ]
    
positions_part = dict(zip(model_folders.keys(), positions_part))

# Run through positions part and create new server, assign it to positions processors:
for k, v in positions_part.items():
    if k == "horizon_24":
        continue
    
    # init controller:    
    control_model_folder = model_folders[k]
    with open(control_model_folder + "/training_parameters.json", "r") as f:
        control_parameters = json.load(f)
    control_parameters["model_file"] = control_model_folder + "/model.hdf5"
    control_parameters["bpso_iterations"] = 25
    control_parameters["bpso_particles"] = 40
    
    # Instanciate bPSO:
    bpso = experiment.dcc.control.BinaryParticleSwarmOptimizer(
        horizon=control_parameters["horizon"], 
        iterations=control_parameters["bpso_iterations"],
        particles=control_parameters["bpso_particles"]
        )
    
    # Now controller:
    controller = experiment.dcc.control.SplitLSTMMPC(
        model_file = control_parameters["model_file"],
        strategy_optimizer=bpso
        )
    
    # Finally, instanciate server & start it:
    control_server = experiment.dcc.server.Server(controller)
    control_server.start()
    
    # Assign to positions:
    for pos_nb in v:
        tot_pos = 0
        for series in xp.series:
            for pos in series.positions:
                if tot_pos == pos_nb:
                    cer = pos.processors["controller"]
                    cer.control_server = control_server
                    cer.horizon = control_parameters["horizon"]
                tot_pos += 1


#%% Run experiment:
platform.neopixel.color(green=1)

xp.run()

#%% Save & clean up:

xp.postprocess()

import pickle
with open(xp.save_folder+"positions_partitioning.pkl","wb") as f:
    pickle.dump(positions_part, f)
with open(xp.save_folder+"controller_model_folders.pkl","wb") as f:
    pickle.dump(model_folders, f)


#%% Switch all back to 24 horizon server to test tp:
pos_nb = positions_part["horizon_24"][0]
tot_pos = 0
for series in xp.series:
    for pos in series.positions:
        if tot_pos == pos_nb:
            control_server = pos.processors["controller"].control_server

for series in xp.series:
    for pos in series.positions:
        pos.processors["controller"].control_server = control_server
        pos.processors["controller"].horizon = 24
        pos.processors["filesaver"].save_path = "E:/JBL/deepmpc/control/2022-05-07_DeepMPC_horizons_tests_cont"

xp.stim_batches = 13
            
#%% Change bpso iterations:
for series in xp.series:
    for pos in series.positions:
        pos.processors["controller"].control_server.controller.strategy_optimizer.max_iterations = 20

#%% Plot all cells results:

# Go through mother cells and sort between ref and non-ref cells:
tpoint = len(xp._acq_timepoints)
tpoint=228
x = np.arange(tpoint)/12
cell_nb = 0
fluo = dict(zip(model_folders.keys(), [[] for _ in range(len(model_folders.keys()))]))
stims = dict(zip(model_folders.keys(), [[] for _ in range(len(model_folders.keys()))]))
for series in xp.series:
    for position in series.positions:
        cer = position.processors["controller"]
        horizon_str = f"horizon_{cer.horizon}"
        for mother in cer.mothers:
            
            fluo[horizon_str].append(mother[:tpoint,cer.features.index("fluo1")])
            stims[horizon_str].append(mother[:tpoint,cer.features.index("stims")])
            cell_nb += 1

import os
os.makedirs(xp.save_folder+"/reference_plots/", exist_ok=True)

import matplotlib.pyplot as plt
plotq = experiment.dcc.utilities.plotq
stims_map = experiment.dcc.utilities.stims_map

for horizon_str in model_folders.keys():
    plt.imshow(stims_map(stims[horizon_str]), aspect='auto')
    plotq(np.array(fluo[horizon_str])*4095)
    plt.plot(x,objectives[0,:tpoint])
    plt.xlabel("time (hours)")
    plt.ylabel("GFP (a.u.)")
    plt.ylim([0, 4095])
    plt.grid(which="both", axis="both")

    error = np.array(fluo[horizon_str])[:,3*12:]*4095-objectives[:len(fluo[horizon_str]),3*12:tpoint]
    rmse = np.sqrt(np.mean(error**2))
    plt.title(f"Control {horizon_str}, rmse = {rmse:.4g}")
    
    
    plt.savefig(xp.save_folder+f"/reference_plots/control_pop_{horizon_str}.png", dpi=300)
    plt.savefig(xp.save_folder+f"/reference_plots/control_pop_{horizon_str}.svg", dpi=300)
    plt.show()
    # Plot fluo response of 100 random cells:
    
    # for i in np.random.choice(len(fluo[horizon_str]), 25, replace = False):
    #     stim_seq = stims[horizon_str][i][:tpoint]
    #     experiment.dcc.utilities.OptoPlotBackground(stim_seq,x, ymax = 4095)
    #     plt.plot(x, objectives[i,:tpoint],"--k")
    #     plt.plot(x, fluo[horizon_str][i]*4095,"b")
    #     plt.xlim([0, tpoint/12])
    #     plt.ylim([0, 4095])
    #     plt.xlabel("time (hours)")
    #     plt.ylabel("GFP (a.u.)")
        
    #     error = np.array(fluo[horizon_str])[i,3*12:]*4095-objectives[0,3*12:tpoint]
    #     rmse = np.sqrt(np.mean(error**2))
        
    #     plt.title(f"Cell #{i}, rmse = {rmse:.4g}")
    #     plt.savefig(xp.save_folder+f"/reference_plots/cell_{horizon_str}_{i:06d}.png", dpi=300)
    #     plt.savefig(xp.save_folder+f"/reference_plots/cell_{horizon_str}_{i:06d}.svg", dpi=300)
    #     plt.show()
