# -*- coding: utf-8 -*-
"""
Created on Sun May  1 17:33:44 2022

@author: jeanbaptiste
"""

import os
import json
import pycromanager_tessie as pmt
from pycromanager_tessie.acquisitions import experiment

# Start platform:
platform = pmt.microscope.platform.Platform(load_synchronizer=True)

# Set LED ring
platform.neopixel.color(red=int(255/4))

# Reproducibility settings:
platform.set("Andor sCMOS Camera","Sensitivity/DynamicRange","12-bit (low noise)")
platform.set("Spectra","White_Level",30)
platform.set("DiaLamp","Intensity",180)

#%% Calibrate DMD:
platform.neopixel.shutter(True)
platform.dmd.calibrate(color = 'blue')
platform.neopixel.shutter(False)

#%% Experimental parameters:

# FALLBACK Control parameters:
control_model_folder = control_model_folder = "C:/Users/System 6/Documents/deepcellcontrol/assets/models/2022-05-07_21-52-16_f651d065-2d7b-4f55-8dee-10e48d6e79b2"
with open(control_model_folder + "/training_parameters.json", "r") as f:
    control_parameters = json.load(f)
control_parameters["model_file"] = control_model_folder + "/model.hdf5"
control_parameters["bpso_iterations"] = 10
control_parameters["bpso_particles"] = 20

# Init experiment object:
xp = experiment.MoMaFeedbackExperiment(
    platform,
    save_folder = "S:/JBL/deepmpc/control/2022-08-04_DeepMPC_tetA_1/",
    delta_config = "C:/Users/System 6/Documents/config_mothermachine_tetA.json",
    control_parameters = control_parameters,
    )
xp.stim_batches = 10

# Channels:
trans_settings = pmt.acquisitions.series.ChannelSettings(
    name='Trans', exposure=100, trigger = "Ext"
    )
gfp_settings = pmt.acquisitions.series.ChannelSettings(
    name='GFP', exposure=100, trigger = "Ext"
    )

# Stimulations:
reddmd_settings = pmt.acquisitions.series.StimulationsSettings(
    color='red', exposure=60, intensity = 100, trigger = "Ext"
    )
greendmd_settings = pmt.acquisitions.series.StimulationsSettings(
    color='green', exposure=100, intensity = 5, trigger = "Ext"
    )


series_settings = pmt.acquisitions.series.SeriesSettings(
    acquisition_mode = 'simultaneous',
    channels = [trans_settings, gfp_settings],
    stimulations = [greendmd_settings, reddmd_settings]
    )



#%% Append new series (run this cell as many times as you want series)

# Create single acquisition series:
series = pmt.acquisitions.series.Series(
    series_number=len(xp.series), platform=platform, settings=series_settings
    )

# Get line of positions:
series.positions_line(index_offset=xp.total_positions())

xp.series.append(series)


#%% Pre-process positions (ie acquire reference images & init processors):  

# Preprocess:
xp.preprocess()

#%% Generate objectives
import numpy as np

# Randomly assign objectives:
objective_levels = (1, 800, 1200, 1800, 4095)
objectives = np.random.choice(
    objective_levels, size=xp.total_chambers(), replace=True,
    ).astype(np.float32)
objectives = np.repeat(objectives[:, np.newaxis], 48*12, axis = 1)
objectives = np.pad(objectives, ((0,0), (36,0)))


np.save(xp.save_folder+"objectives.npy", objectives)

# Assign to cells:
xp.assign_objectives(objectives)

#%% Probably not necessary anymore:

# Set up lamp:
platform.dmd.set_channel(
    "red", 
    exposure=60, 
    intensity=100, 
    time_unit="ms", 
    trigger="Ext"
    )

platform.dmd.set_channel(
    "green", 
    exposure=100,
    intensity=5,
    time_unit="ms", 
    trigger="Ext"
    )

platform.synchronizer.set_camera()
platform.synchronizer.set_fluo_lamp()
platform.synchronizer.set_dmd()



#%% Run experiment:

platform.neopixel.color(red=int(255/4))
xp.run()

#%% Save & clean up:

xp.postprocess()

platform.synchronizer.unset_camera()
platform.synchronizer.unset_fluo_lamp()
platform.synchronizer.unset_dmd()

#%% Quickly plots:


# Cells data:
import pickle
with open(xp.save_folder+"/mothers.pkl", "rb") as f:
    mothers = pickle.load(f)
    

# Compute RMSE per cell (after 3+3 hours):
comp_start = 6*12
cutoff = 180
local_cells_rmse = []
cells_fluo = []
cells_stims = []
x = np.arange(cutoff)/12
cell_nb = 0
for s in mothers:
    for p in s:
        for mother in p:
            fluo = mother[:cutoff, 0]*4095
            obj = objectives[cell_nb, :cutoff]
            stims = mother[:cutoff, -1]*4095
            rmse = np.sqrt(np.mean((fluo[comp_start:]-obj[comp_start:])**2))
            cell_nb += 1
            
            cells_fluo.append(fluo)
            cells_stims.append(stims)
            local_cells_rmse.append(rmse)

cells_fluo = np.array(cells_fluo)
cells_stims = np.array(cells_stims)
local_cells_rmse = np.array(local_cells_rmse)
np.save(xp.save_folder+"/cells_rmse.npy", local_cells_rmse)
np.save(xp.save_folder+"/cells_fluo.npy", cells_fluo)
np.save(xp.save_folder+"/cells_stims.npy", cells_stims)


os.makedirs(xp.save_folder+"/reference_plots/", exist_ok=True)
import matplotlib.pyplot as plt

cm = plt.get_cmap('gist_rainbow')
line = lambda x: plt.plot((x/12,x/12),(0,4095),'--', color="#808080")
line(108)
# line(144)
for ol, obj_lvl in enumerate(objective_levels):
    color = cm(ol/len(objective_levels), alpha=None)
    plt.plot([36/12, cutoff/12], [obj_lvl, obj_lvl], "--", color = color)
    fluo = cells_fluo[objectives[:, -1]==obj_lvl, :]
    experiment.dcc.utilities.plotq(fluo, color = color)

plt.xlim([0, cutoff/12])
# plt.ylim([0, 1])
plt.xlabel("time (hours)")
plt.ylabel("GFP (a.u.)")
plt.savefig(xp.save_folder+f"/reference_plots/control_results.png", dpi=300)
plt.savefig(xp.save_folder+f"/reference_plots/control_results.svg", dpi=300)
plt.show()

for i in np.random.choice(len(cells_fluo), 100, replace = False):
    stim_seq = cells_stims[i]
    experiment.dcc.utilities.OptoPlotBackground(stim_seq,x, ymax = 1)
    plt.plot(x, objectives[i,:cutoff]/4095,"--k")
    plt.plot(x, cells_fluo[i]/4095,"b")
    line(108)
    # line(144)
    plt.xlim([0, cutoff/12])
    plt.ylim([0, 1])
    plt.xlabel("time (hours)")
    plt.ylabel("GFP (a.u.)")
    plt.title(f"Cell #{i}, RMSE = {local_cells_rmse[i]:.2f}")
    plt.savefig(xp.save_folder+f"/reference_plots/cell_{i:06d}.png", dpi=300)
    plt.savefig(xp.save_folder+f"/reference_plots/cell_{i:06d}.svg", dpi=300)
    plt.show()
