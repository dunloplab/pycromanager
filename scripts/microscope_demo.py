"""
This script demonstrates some basic functionalities of the microscope 
 sub-package. That package deals with the hardware side of things: snap images,
 retrieve them, change equipment settings, load images into DMD etc...
"""

import time

import cv2
import numpy as np
import matplotlib.pyplot as plt

#%% Load platform:
import pycromanager_tessie as pmt

# Start platform (without sync arduino):
platform = pmt.microscope.platform.Platform(load_synchronizer=False)

#%% LED ring:

# Increasing instensity:
for i in range(256):
    time.sleep(.02)
    platform.neopixel.color(blue=i)

# The shutter turns the light on/off but keeps the old color in memory:
for i in range(50):
    platform.neopixel.shutter(True) # Shutter ON, Lights OFF
    time.sleep(.05)
    platform.neopixel.shutter(False) # Shutter OFF, Lights ON
    time.sleep(.05)

for i in range(256):
    time.sleep(.02)
    platform.neopixel.color(blue=255-i, red = i)

for i in range(50):
    platform.neopixel.shutter() # If no state given, shutter simply inverts
    time.sleep(.05)
    platform.neopixel.shutter()
    time.sleep(.05)

for i in range(256):
    time.sleep(.02)
    platform.neopixel.color(red = 255-i)

#%% Snap images, change exposure etc...

# Set up trans channel:
platform.channel("Trans")
platform.exposure(20)

# Trigger camera acquisition:
platform.snap()

# Retrieve image and display it:
I = platform.retrieve()
plt.imshow(I)
plt.show()

# Same thing with GFP:
platform.channel("GFP")
platform.exposure(200)
platform.snap()
I = platform.retrieve()
plt.imshow(I)
plt.show()

# You can acquire up to 32 (I think?) frames before retrieving them:
for _ in range(32):
    platform.snap()
    time.sleep(.1)

I = platform.retrieve()
for ind in range(31):
    
    # Retrieve image:
    new_I = platform.retrieve()
    
    # Check if a new image was indeed retrieved:
    if np.all(new_I-I==0):
        print(f"Frames {ind} and {ind+1} are identical")
    else:
        print(f"Frames {ind+1} is a new, different image")

    # Display differences:
    plt.imshow(I-new_I)
    plt.show()
    
    I = new_I

# Turn on live acquisition mode:
platform.live()

# Turn off:
platform.live(False)

#%% Move along XY

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#                                  IMPORTANT
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# This will move the XY platform around a little bit. Make sure the objective
# will not bump into anything.

# Check where we are:
pos = platform.where()
print(pos)

# Move by 100 um to the right:
new_xy = (pos["xy"][0]+100, pos["xy"][1])
platform.move(xy = new_xy)

# Move back to original position:
platform.move(xy = pos["xy"])

#%% Move along Z

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#                                  IMPORTANT
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# This will move the objective up and down a little bit. Make it will not bump
# into anything.

# Check where we are:
pos = platform.where()
print(pos)

# Move down by 20 um:
platform.move(z = pos["z"]-20)

#%% Autofocus:

# Single-shot:
platform.focus()

# Continuous focus:
platform.focus(mode = "continuous")
# Note: continuous focus is deactivated if z is changed with platform.move.

# To wait for continuous focus to converge:
pos = platform.where()
new_xy = (pos["xy"][0]+100, pos["xy"][1])
platform.move(xy = new_xy)
platform.focus() # This will block until the PFS has converged

# Turn continuous focus back off:
platform.focus(mode = "off")

#%% Set equipment parameters (see device properties browser in MM interface)

# Get Trans lamp intensity:
intensity = platform.get("DiaLamp","Intensity")
print(intensity)

# Get all device properties:
dev_props = platform.get_all_properties()

# Set fluo lamp intensity:
platform.set("Spectra","White_Level",50)

# Set camera bit depth:
platform.set("Andor sCMOS Camera","Sensitivity/DynamicRange","12-bit (low noise)")

# Set core imaging shutter to trans lamp:
platform.set("Core","Shutter","DiaLamp")

# An interesting application: Acquiring both trans and gfp without changing
# filters:
platform.channel("GFP")
platform.exposure(50)
platform.set("Core","Shutter","DiaLamp")
platform.snap()
platform.set("Core","Shutter","Spectra")
platform.snap()
trans_img = platform.retrieve()
plt.imshow(trans_img)
plt.show()
gfp_img = platform.retrieve()
plt.imshow(gfp_img)
plt.show()

#%% DMD

# Create image:
mkms = np.zeros(shape=(684,608), dtype=np.uint8) # (608,684) Shape of DMD array
cv2.circle(mkms, (304,342), 110, 255, -1)
cv2.circle(mkms, (204,242), 75, 255, -1)
cv2.circle(mkms, (404,242), 75, 255, -1)
plt.imshow(mkms)
plt.show()

# Send to DMD:
platform.dmd.set_image(mkms)

# Set up DMD channel:
platform.dmd.set_channel("blue", exposure=1, intensity=5)

# Turn on live mode:
platform.live()

# Calibrate DMD:
platform.dmd.calibrate()

# New (desired) image:
mkms = np.zeros(shape=(2048,2048), dtype=np.uint8) # (608,684) Shape of DMD array
cv2.circle(mkms, (1024,1024), 300, 255, -1)
cv2.circle(mkms, (724,724), 220, 255, -1)
cv2.circle(mkms, (1324,724), 220, 255, -1)
plt.imshow(mkms)
plt.show()

# Infer what image to shine to get this result:
mkms = platform.dmd.infer_image(mkms)
plt.imshow(mkms)
plt.show()

# Show result:
platform.dmd.set_image(mkms)
platform.dmd.set_channel("blue", exposure=1, intensity=5)
platform.live()

#%% DMD sequences

# Create sequence of images to load into DMD at once:
mkms = np.zeros(shape=(2048,2048), dtype=np.uint8) # (608,684) Shape of DMD array
cv2.circle(mkms, (1024,1024), 300, 255, -1)
cv2.circle(mkms, (724,724), 220, 255, -1)
cv2.circle(mkms, (1324,724), 220, 255, -1)

images  = []
rot_mat = cv2.getRotationMatrix2D((1024,1024), 1, 1.0)
for _ in range(360):
    mkms = cv2.warpAffine(mkms, rot_mat, mkms.shape[::-1])
    images.append(platform.dmd.infer_image(mkms))

# Load sequence into DMD:
platform.dmd.sequence(images, color="blue", exposure=1, trigger="DC", intensity=5)

# Turn on live mode:
platform.dmd.set_channel("blue", exposure=1)
platform.live()

# Run through frames:
while True:
    platform.dmd.next_frame()
    time.sleep(.05)

# Stop DMD sequence acquisition:
platform.dmd.stop_sequence()


# Re-load sequence into DMD, in internal trigger mode:
platform.dmd.sequence(images, color="blue", exposure=50, trigger="Int", intensity=5)

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#                                  IMPORTANT
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Once in "Int" or "Ext" mode (or anything other than "DC"), do !not! click the
# snap or live button in micromanager, or use the snap or live method of the
# platform. This will completely freeze micromanager.

# Run through frames:
while True:
    platform.dmd.next_frame()
    platform.dmd.shine()
    time.sleep(.05)

# Stop DMD sequence acquisition:
platform.dmd.stop_sequence()
# Now you can use the snap and live buttons again.