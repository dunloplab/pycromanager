"""
To acquire stacks of images with the fluo lamp on, for the fluorophore
classification project.
"""
import os
import time
import json

import cv2
import numpy as np
import matplotlib.pyplot as plt

#%% Load platform:
import pycromanager_tessie as pmt

# Start platform (without sync arduino):
platform = pmt.microscope.platform.Platform(load_synchronizer=False, load_dmd=False)

positions = []

platform.set("Andor sCMOS Camera","Sensitivity/DynamicRange","12-bit (low noise)")
platform.set("Andor sCMOS Camera","SensorCooling","Off")
platform.set("Spectra","White_Level",10)

#%% Record positions:

xyz = platform.where()
name = input("Position name? ")
name = name.strip()

positions.append({"name":name, "xyz":xyz})

#%% Settings
trans_exp = 20
rfp_exp = 40
gfp_cfp_exp = 3


intervals = [20, 50, 100]
num_imgs = 32

period = 15*60

#%% Run acquisition:

next_timepoint = time.time()
proto = 'S:/Caroline/2023-04-13_FPselection3/{pos["name"]}/{channel}/Timepoint{timepoint:06d}_interval{interval:03d}_index{i:02d}.tif'
properties = platform.get_all_properties()
with open("S:/Caroline/2023-04-13_FPselection3/platform_properties.json", "w") as f:
    json.dump(properties, f, indent=2)

timepoint = -1
while True:
    
    if time.time() < next_timepoint:
        time.sleep(1)
        continue
    
    next_timepoint = time.time() + period
    timepoint += 1
    
    for pos in positions:
        
        print(time.strftime('%H:%M:%S') + f" - Acquiring position {pos['name']}")
        platform.move(pos["xyz"]["xy"],pos["xyz"]["z"])
        platform.focus(mode = "continuous")
        
        for interval in intervals:
            i = 0
            interval_str = f"Interval_{interval:03d}"
            
            print(f"{time.strftime('%H:%M:%S')} - Interval {interval} ms")
        
            
            # Acquire trans image:
            channel = "Trans"
            platform.channel(channel)
            platform.exposure(trans_exp)
            platform.snap()
            I = platform.retrieve()
            plt.imshow(I)
            plt.title(f"Position {pos['name']} - Timepoint {timepoint} - Interval {interval} ms")
            plt.show()
            filename = eval(f"f'{proto}'")
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            cv2.imwrite(filename, I)
            
            # Acquire RFP image:
            channel = "CFP"
            platform.channel(channel)
            platform.exposure(rfp_exp)
            platform.snap()
            I = platform.retrieve()
            filename = eval(f"f'{proto}'")
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            cv2.imwrite(filename, I)
            
            # Acquire GFP and CFP sequences:
            for channel in (["RFP"]):
                
                
                frametimes = []
                
                platform.channel(channel)
                platform.exposure(gfp_cfp_exp)
                
                platform.mmc.set_auto_shutter(False)
                platform.mmc.set_shutter_open(True)
                
                # Acquire images:
                for i in range(num_imgs): 
                    frametimes += [time.time()]
                    platform.snap()
                    while time.time() < frametimes[-1] + interval/1000:
                        time.sleep(.0001)
                
                platform.mmc.set_shutter_open(False)
                platform.mmc.set_auto_shutter(True)
                
                # Save images to disk:
                for i in range(num_imgs):
                    I = platform.retrieve()
                    filename = eval(f"f'{proto}'")
                    os.makedirs(os.path.dirname(filename), exist_ok=True)
                    cv2.imwrite(filename, I)
                
                # Write acquisition timepoints to JSON:
                if timepoint == 0 and interval == 20:
                    file_data = {interval_str: []}
                else:
                    with open(os.path.dirname(filename)+"/acquisition_times.json",'r') as file:
                        file_data = json.load(file)
                if interval_str not in file_data:
                    file_data[interval_str] = []
                file_data[interval_str].append(frametimes)
                with open(os.path.dirname(filename)+"/acquisition_times.json",'w') as file:
                    json.dump(file_data, file, indent = 2)
            
                # Make sure camera buffer is flushed
                I_new = platform.retrieve()
                while not np.array_equal(I,I_new):
                    I = I_new
                    I_new = platform.retrieve()
            
            # Record last Z position:
            pos["xyz"]["z"] = platform.where()["z"]
