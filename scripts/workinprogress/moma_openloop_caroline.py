# -*- coding: utf-8 -*-
"""
Created on Sun May  1 17:33:44 2022

@author: jeanbaptiste
"""

import os
import json
import pycromanager_tessie as pmt
from pycromanager_tessie.acquisitions import experiment

# Start platform:
platform = pmt.microscope.platform.Platform(load_synchronizer=True)

# Set LED ring
platform.neopixel.color(red=int(255/4))

# Reproducibility settings:
platform.set("Andor sCMOS Camera","Sensitivity/DynamicRange","12-bit (low noise)")
platform.set("Andor sCMOS Camera","SensorCooling","Off")
platform.set("Spectra","White_Level",30)
platform.set("DiaLamp","Intensity",180)


#%% Calibrate DMD:

platform.neopixel.shutter(True)
platform.dmd.calibrate(color = 'blue')
platform.neopixel.shutter(False)

#%% Experimental parameters:

# Init experiment object:
xp = experiment.MoMaOpenLoopExperiment(
    platform,
    save_folder = "S:/Caroline/2022-04-05_gad_test2",
    delta_config = "C:/Users/System 6/Documents/config_mothermachine_tetA.json",
    features = ("fluo1","fluo2","area","stims"),
    )
xp.stim_batches = 10

#%%
# Channels:
trans_settings = pmt.acquisitions.series.ChannelSettings(
    name='Trans', exposure=10, trigger = "Ext"
    )
gfp_settings = pmt.acquisitions.series.ChannelSettings(
    name='GFP', exposure=10, trigger = "Ext"
    )
rfp_settings = pmt.acquisitions.series.ChannelSettings(
    name='RFP', exposure=10, trigger = "Ext"
    )

# Stimulations:
reddmd_settings = pmt.acquisitions.series.StimulationsSettings(
    color='red', exposure=60, intensity = 100, trigger = "Ext"
    )

series_settings = pmt.acquisitions.series.SeriesSettings(
    acquisition_mode = 'channels_sequence',
    channels = [trans_settings, gfp_settings, rfp_settings],
    stimulations = [reddmd_settings]
    )


#% Append new series (run this cell as many times as you want series)

# Create single acquisition series:
series = pmt.acquisitions.series.Series(
    series_number=len(xp.series), platform=platform, settings=series_settings
    )

# series._hooks["pre"] = lambda : platform.set("Spectra","White_Level",20)

# Get line of positions:
series.positions_line(index_offset=xp.total_positions())

xp.series.append(series)


#%% Pre-process positions (ie acquire reference images & init processors):  

# Preprocess:
xp.preprocess()

#%% Generate stimulations
import numpy as np

# Assign half of the cells to be stimulated after 3 hours:
cells_stims = np.zeros((xp.total_chambers(),48*12),dtype=bool)
activate = np.random.randint(0,5,cells_stims.shape[0])

cells_stims[np.where(activate==0), :] = False # All red
cells_stims[np.where(activate==1), ::4] = True # 1/4 green
cells_stims[np.where(activate==2), ::2] = True # 1/2 green
cells_stims[np.where(activate==3), ::] = True
cells_stims[np.where(activate==3), ::4] = False # 3/4 green
cells_stims[np.where(activate==4), ::] = True # All green

cells_stims[:, :24] = False
# cells_stims[:, (8*12):] = np.logical_not(cells_stims[:, (8*12):])

xp.assign_stimulations(cells_stims.astype(int))

np.save(xp.save_folder+"/cells_stims.npy", cells_stims)

#%%

# Set up lamp:
platform.dmd.set_channel(
    "red", 
    exposure=60, 
    intensity=100, 
    time_unit="ms", 
    trigger="Ext"
    )

platform.dmd.set_channel(
    "green", 
    exposure=100,
    intensity=5,
    time_unit="ms", 
    trigger="Ext"
    )

platform.synchronizer.set_camera()
platform.synchronizer.set_fluo_lamp()
platform.synchronizer.set_dmd()


#%% Run experiment:

platform.neopixel.color(red=int(255/2))
xp.total_timepoints = 2*12
xp.run()

platform.neopixel.color(red=0)
xp.total_timepoints = 48*12
xp.run()


#%% Save & clean up:

xp.postprocess()


platform.synchronizer.unset_camera()
platform.synchronizer.unset_fluo_lamp()
platform.synchronizer.unset_dmd()

#%% Quick plots:


# Cells data:
import pickle
import matplotlib.pyplot as plt

with open(xp.save_folder+"/mothers.pkl", "rb") as f:
    mothers = pickle.load(f)
 
# with open("S:/Caroline/2022-02-02_Highthroughput_random_5_inverter134_135"+"/mothers.pkl", "rb") as f:
#     mothers = pickle.load(f)

os.makedirs(xp.save_folder+"/reference_plots/", exist_ok=True)

first_cutoff = 20
cutoff = 299

#Compute RMSE per cell (after 3+3 hours):
for cat in range(5):
    cell_nb = 0
    all_gfp = []
    all_rfp = []
    
    for s, series in enumerate(mothers):
        
        if s == 2 or s == 3:
        
            for _p, p in enumerate(series):
                print(f"\npos {_p}:", end=" ")
                for m, mother in enumerate(p):
                    gfp = mother[first_cutoff:cutoff, 0]
                    rfp = mother[first_cutoff:cutoff, 1]
                    #stims = mother[first_cutoff:cutoff, -1]
                    stims = mother[0:cutoff, -1]
                    
                    if sum(stims[24:28]) == cat:
                        all_gfp += [gfp]
                        all_rfp += [rfp]
                    cell_nb += 1
        
    

# cell_nb = 0
# all_gfp = []
# all_rfp = []

# cat = 0
        
# for s, series in enumerate(mothers):
            
#     if s > 1:
            
#         for _p, p in enumerate(series):
#             print(f"\npos {_p}:", end=" ")
#             for m, mother in enumerate(p):
#                 gfp = mother[first_cutoff:cutoff, 0]
#                 rfp = mother[first_cutoff:cutoff, 1]
#                 stims = mother[0:cutoff, -1]
                        
#                 if sum(stims[12:16]) == cat:
#                     all_gfp += [gfp]
#                     all_rfp += [rfp]
#                     cell_nb += 1
            
    
    # experiment.dcc.utilities.plotq(all_gfp, color = "g")
    # experiment.dcc.utilities.plotq(all_rfp, color = "r")
    
    #plt.scatter(all_gfp, all_rfp,)
    
    # plt.title(str(cat)+' green stimulations, strain 152')
    # plt.ylim((0,4095))
    # plt.xlabel("Time")
    # plt.ylabel("Fluo")

    # plt.ylabel("RFP (A.U)")
    # plt.xlabel("GFP (A.U.)")

    # plt.show()
    
    
    
    
def compute_growth(area):
    
    # Run through time:
    growth = []
    for t in range(len(area)-1):
    
        cont_flag = False
    
        # If current area is None or too small, append a NaN (empty chamber)
        if area[t] is None or area[t] < 100 :
            growth.append(np.nan)
            continue
        
        # Same thing for time t+1:
        if area[t+1] is None or area[t+1] < 100:
            growth.append(np.nan)
            continue
        
        # Otherwise compute growth as delta_area / area:
        growth.append((area[t+1] - area[t])/area[t])
    
    # Filter out divisions and glitches:
    growth = np.array(growth)
    growth[growth<-.2] = np.nan
    growth[growth>.3] = np.nan
    
    # Convert to 1/hour:
    growth*=12
    
    return growth, area

area = []
stims = []

s = mothers[3]

for p in s:
    for c in p:
        area.append(c[:cutoff,xp.features.index("area")])
        stims.append(c[:cutoff,xp.features.index("stims")])
area = np.array(area)
stims = np.array(stims)


growth = np.zeros_like(area)[:,:-1]
for c in range(area.shape[0]):
    _g, _a = compute_growth(area[c])
    growth[c] = _g
    area[c] = _a


window = (6, 6)
avg_growth = np.zeros_like(growth)

for f in range(cutoff-1):
        
        f_min = f-window[0]
        if f_min < 0: f_min = 0
        f_max = f+window[1]
        if f_max > cutoff: f_max = cutoff
        
        t_growth = np.nanmean(growth[:,f_min:f_max], axis=1)
        avg_growth[:,f] = t_growth


import matplotlib.pyplot as plt

def plotq(fluo,q = .5, color="b"):
    
    x = np.arange(0,len(fluo[0])/12,1/12)
    plt.plot(x,np.nanmedian(fluo,axis=0), color=color)
    plt.fill_between(
        x,
        np.nanquantile(fluo,.5-q/2,axis=0),
        np.nanquantile(fluo,.5+q/2,axis=0),
        color=color,
        alpha=.2,
        )

fours = np.full((len(stims),cutoff-1), False)
threes = np.full((len(stims),1), False)
twos = np.full((len(stims),1), False)
ones = np.full((len(stims),1), False)
zees = np.full((len(stims),cutoff-1), False)

for i in range(len(stims)):
    stimvec = stims[i]
    if sum(stimvec[24:28])==4:
        fours[i,:] = True
    if sum(stimvec[24:28])==3:
        threes[i] = True
    if sum(stimvec[24:28])==2:
        twos[i] = True
    if sum(stimvec[24:28])==1:
        ones[i] = True
    if sum(stimvec[24:28])==0:
        zees[i,:] = True

plotq(growth[fours[:,0],:], color = 'g')
plotq(growth[zees[:,0],:], color = 'r')
plt.xlabel("time (hours)")
plt.ylabel("Growth rate (1/hour)")
#plt.savefig(xp.save_folder + "Growth_plot_high-throughput_NoGFP.png", dpi=300) 
