# -*- coding: utf-8 -*-
"""
Created on Sun May  1 17:33:44 2022

@author: jeanbaptiste
"""

import os
import json
import pycromanager_tessie as pmt
from pycromanager_tessie.acquisitions import experiment

# Start platform:
platform = pmt.microscope.platform.Platform(load_synchronizer=True)

# Set LED ring
platform.neopixel.color(green=int(1))  #Set to 0 or green1

# Reproducibility settings:
platform.set("Andor sCMOS Camera","Sensitivity/DynamicRange","12-bit (low noise)")
platform.set("Spectra","White_Level",30)
platform.set("DiaLamp","Intensity",180)

platform.set("Andor sCMOS Camera","SensorCooling","Off")

#%% Calibrate DMD:
platform.neopixel.shutter(True)
platform.dmd.calibrate(color = 'blue')
platform.neopixel.shutter(False)

#%% Experimental parameters:

# Init experiment object:
xp = experiment.MoMaFluoSelectionExperiment(
    platform,
    save_folder = "S:/Caroline/2023-05-19_Selection_tetA_11/",
    delta_config = "C:/Users/System 6/Documents/config_mothermachine_tetA.json",
    features = ("area", "length", "stims")
    )
xp.stim_batches = 10
xp.period = 300
xp.slow_return = .5 # This is the "dwell time" on each intermediate position if you want to go faster or slower.

# Channels:
trans_settings = pmt.acquisitions.series.ChannelSettings(
    name='Trans', exposure=100, trigger = "Ext"
    )
gfp_settings = pmt.acquisitions.series.ChannelSettings(
    name='GFP', exposure=25, trigger = "Ext"
    )

# Stimulations:
reddmd_settings = pmt.acquisitions.series.StimulationsSettings(
    color='red', exposure=60, intensity = 100, trigger = "Ext"
    ) #Was 60 exposure


# series_settings = pmt.acquisitions.series.SeriesSettings(
#     acquisition_mode = 'simultaneous',
#     channels = [trans_settings, gfp_settings],
#     stimulations = [greendmd_settings, reddmd_settings]
#     )

series_settings = pmt.acquisitions.series.SeriesSettings(
    acquisition_mode = 'channels_sequence',
    channels = [trans_settings],
    stimulations = [reddmd_settings]
    )

#%% Append new series (run this cell as many times as you want series)

#Do the 3 different series (1 per size)

# Create single acquisition series:
series = pmt.acquisitions.series.Series(
    series_number=len(xp.series), platform=platform, settings=series_settings
    )

# Get line of positions:
series.positions_line(index_offset=xp.total_positions())

xp.series.append(series)

#%% Pre-process positions (ie acquire reference images & init processors):
# TODO display preprocessing for delta
rfp_settings = pmt.acquisitions.series.ChannelSettings(
    name='RFP', exposure=10, trigger = "Int"
    )

xp.preprocess(rfp_settings, threshold=400)

#%% Probably not necessary anymore:

# Set up lamp:
platform.dmd.set_channel(
    "red", 
    exposure=60, 
    intensity=100, 
    time_unit="ms", 
    trigger="Ext"
    )

platform.dmd.set_channel(
    "green", 
    exposure=0,
    intensity=5,
    time_unit="ms", 
    trigger="Ext"
    )

platform.synchronizer.set_camera()
platform.synchronizer.set_fluo_lamp()
platform.synchronizer.set_dmd()



#%% Run experiment:

xp.run()

#%% Acquire GFP images on last timepoint:

gfp_settings = pmt.acquisitions.series.ChannelSettings(
    name='GFP', exposure=25, trigger = "Ext"
    )
rfp_settings = pmt.acquisitions.series.ChannelSettings(
    name='RFP', exposure=10, trigger = "Ext"
    )
for series in xp.series:
    series.settings.channels.append(gfp_settings)
    series.settings.channels.append(rfp_settings)
    for position in series.positions:
        position.processors["filesaver"].add_channel("GFP", watch=True)
        position.processors["controller"].add_channel("GFP", watch=False)
        position.processors["filesaver"].remove_channel("RFP")
        position.processors["filesaver"].add_channel("RFP", watch=True)
        position.processors["controller"].remove_channel("RFP")
        position.processors["controller"].add_channel("RFP", watch=False)
xp.features = ("area", "length", "stims", "fluo1", "fluo2")

#%% Run 1 more acq timepoint:
xp.total_timepoints = len(xp._acq_timepoints) + 1
xp.run()

#%% Remove GFP and RFP imaging?
for series in xp.series:
    series.settings.channels.remove(gfp_settings)
    series.settings.channels.remove(rfp_settings)
    for position in series.positions:
        position.processors["filesaver"].remove_channel("RFP")
        position.processors["filesaver"].remove_channel("GFP")
        position.processors["controller"].remove_channel("RFP")
        position.processors["controller"].remove_channel("GFP")

#%% Assign blue "killing" stimulations
exposures = [1000, 3000, 10_000]
num_timepoints = len(xp._acq_timepoints)

for series, exp in zip(xp.series, exposures):
    bluedmd_settings = pmt.acquisitions.series.StimulationsSettings(
        color='blue', exposure=exp, intensity = 100, trigger = "Ext"
        )
    series.settings.stimulations = [bluedmd_settings]
    
    for position in series.positions:
        selector = position.processors["controller"]
        stim_ind = selector.features.index('stims')
        gfp_ind = selector.features.index('fluo2') # Double check that GFP is fluo2
        
        # Run through mothers in position, update dmd stimulations:
        for m, mother in enumerate(selector.mothers):
            
            # Check if cell is selected:
            stim = mother[num_timepoints,stim_ind]
            if stim == 1: # Selected for green stim, nothing to do
                continue
            
            # Check if GFP is too high:
            gfp = mother[num_timepoints,gfp_ind] # Double check that GFP is stored at num_timepoints in mothers
            if gfp > 500:
                mother[:,stim_ind] = 2

#%% Run 1 more acq timepoint:
xp.total_timepoints = len(xp._acq_timepoints) + 1
xp.run()

#%% Save & clean up:

xp.postprocess()

platform.synchronizer.unset_camera()
platform.synchronizer.unset_fluo_lamp()
platform.synchronizer.unset_dmd()

#%%
import pickle
import numpy as np

with open(xp.save_folder+"mothers.pkl","rb") as f:
    mothers = pickle.load(f)
    
# with open("S:/Caroline/2023-04-18_Selection_tetA_4/mothers.pkl","rb") as f:
#      mothers = pickle.load(f)

cutoff = 278


def compute_growth(area):
    
    # Run through time:
    growth = []
    for t in range(len(area)-1):
    
        cont_flag = False
    
        # If current area is None or too small, append a NaN (empty chamber)
        if area[t] is None or area[t] < 100 :
            growth.append(np.nan)
            continue
        
        # Same thing for time t+1:
        if area[t+1] is None or area[t+1] < 100:
            growth.append(np.nan)
            continue
        
        # Otherwise compute growth as delta_area / area:
        growth.append((area[t+1] - area[t])/area[t])
    
    # Filter out divisions and glitches:
    growth = np.array(growth)
    growth[growth<-.2] = np.nan
    growth[growth>.3] = np.nan
    
    # Convert to 1/hour:
    growth*=12
    
    return growth, area

area = []
stims = []
for s in mothers:
    for p in s:
        for c in p:
            area.append(c[:cutoff,xp.features.index("area")])
            stims.append(c[:cutoff,xp.features.index("stims")])
area = np.array(area)
stims = np.array(stims)


growth = np.zeros_like(area)[:,:-1]
for c in range(area.shape[0]):
    _g, _a = compute_growth(area[c])
    growth[c] = _g
    area[c] = _a


window = (6, 6)
avg_growth = np.zeros_like(growth)

for f in range(cutoff-1):
        
        f_min = f-window[0]
        if f_min < 0: f_min = 0
        f_max = f+window[1]
        if f_max > cutoff: f_max = cutoff
        
        t_growth = np.nanmean(growth[:,f_min:f_max], axis=1)
        avg_growth[:,f] = t_growth


import matplotlib.pyplot as plt

def plotq(fluo,q = .5, color="b"):
    
    x = np.arange(0,len(fluo[0])/12,1/12)
    plt.plot(x,np.nanmedian(fluo,axis=0), color=color)
    plt.fill_between(
        x,
        np.nanquantile(fluo,.5-q/2,axis=0),
        np.nanquantile(fluo,.5+q/2,axis=0),
        color=color,
        alpha=.2,
        )

plotq(growth[stims[:,0]==1, :], color = 'g')
plotq(growth[stims[:,0]==0, :], color = 'r')
plt.xlabel("time (hours)")
plt.ylabel("Growth rate (1/hour)")
plt.savefig(xp.save_folder + "Growth_plot_high-throughput_NoGFP.png", dpi=300)